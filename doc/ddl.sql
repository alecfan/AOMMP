/*==============================================================*/
/* Table: INVENTORY_ISO_SERVER                                  */
/*==============================================================*/
create table INVENTORY_ISO_SERVER 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   SERVERURL            varchar(200)                   not null,
   IP                   varchar(15)                    not null,
   SERVERTYPE           int                            not null,
   SERVERSTATUSMARK     varchar(20)                    not null,
   UPDATETIME           datetime                       null,
   constraint PK_INVENTORY_ISO_SERVER primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci; 

/*==============================================================*/
/* Table: INVENTORY_ISO                                         */
/*==============================================================*/
create table INVENTORY_ISO 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   ISONAME              varchar(100)                   not null,
   ISOTYPE              varchar(10)                    null,
   ISOVERSION           varchar(10)                    null,
   ISOUSERVERSION       varchar(50)                    null,
   STATUS_BIT		varchar(2)                    null,
   ISOBIT               int                            null,
   USERACCOUNT          varchar(100)                   not null,
   CREATETIME           datetime                       not null,
   USEAIM               varchar(500)                   null,
   constraint PK_INVENTORY_ISO primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci; 

/*==============================================================*/
/* Table: INVENTORY_KS                                          */
/*==============================================================*/
create table INVENTORY_KS 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   KSNAME               varchar(100)                   not null,
   ISOCSAID             int                            not null,
   USERACCOUNT          varchar(100)                   not null,
   CREATETIME           datetime                       not null,
   UPDATETIME           timestamp                      not null,
   constraint PK_INVENTORY_KS primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci; 

/*==============================================================*/
/* Table: INVENTORY_SCHEME                                      */
/*==============================================================*/
create table INVENTORY_SCHEME 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   KSCSAID              int                            not null,
   SCHEMENAME           varchar(100)                   not null,
   SCHEMESTATUS         int			       not null,
   INSTALLSTATUS         int			       not null,
   USERACCOUNT          varchar(100)                   not null,
   CREATETIME           datetime                       not null,
   INSTALLSUM           int                            null,
   INSTALLFINISHED      int                            null,
   INSTALLING           int                            null,
   INSTALLERROR         int                            null,
   constraint PK_INVENTORY_SCHEME primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci; 

/*==============================================================*/
/* Table: INVENTORY_IP                                          */
/*==============================================================*/
create table INVENTORY_IP 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   MAC                  varchar(17)                    not null,
   IPADDRESS            varchar(28)                    not null,
   MASK                 varchar(15)                    not null,
   BATCHID              varchar(100)                   not null,
   USERACCOUNT          varchar(100)                   not null,
   CREATETIME           datetime                       not null,
   IPSTATUS             varchar(1000)                    not null,
   PERCENT              int                            null,
   MIRRORLIBRARYID		INT							   null,
   constraint PK_INVENTORY_IP primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci;

/*==============================================================*/
/* Table: LOG_INVENTORY_ISO                                     */
/*==============================================================*/
create table LOG_INVENTORY_ISO 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   USERACCOUNT          varchar(100)                   not null,
   UPDATETIME           timestamp                      not null,
   USERIP               varchar(15)                    null,
   NOTES                varchar(500)                   not null,
   ISOCSAID             int                            not null,
   constraint PK_LOG_INVENTORY_ISO primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci;

/*==============================================================*/
/* Table: LOG_OS_IP                                             */
/*==============================================================*/
create table LOG_OS_IP 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   USERACCOUNT          varchar(100)                   not null,
   UPDATETIME           datetime                       not null,
   USERIP               varchar(15)                    null,
   NOTES                varchar(500)                   not null,
   IPCSAID              int                            not null,
   constraint PK_LOG_OS_IP primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci;


/*==============================================================*/
/* Table: TASK_ISO                                           */
/*==============================================================*/
create table TASK_ISO 
(
   TID INT NOT NULL AUTO_INCREMENT,
   TASKSTATUS           int                            not null,
   TASKTYPE             int                            not null,
   TASKMSG              varchar(2000)                  null,
   MEDIACSAID           int                            null,
   NOTICESTATUS         int                            not null,
   SCHEMECSAID          int                            null,
   CREATETIME           datetime                       not null,
   ENDTIME              datetime                       null,
   WAREHOUSESERVER      int                            null,
   constraint PK_TASK_UPLOAD primary key (TID)
)CHARSET=utf8 COLLATE=utf8_general_ci;


/*==============================================================*/
/* Table: REL_SHEll_KS                                         */
/*==============================================================*/
create table REL_SHEll_KS 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   KSCSAID             int                            not null,
   SHELLCSAID           int                            not null,
   constraint PK_REL_SHEll_KS primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci;

/*==============================================================*/
/* Table: REL_SCHEME_IP                                         */
/*==============================================================*/
CREATE TABLE REL_SCHEME_IP 
(
   CSAID                INT                    NOT NULL AUTO_INCREMENT,
   SCHCSAID             INT                    NOT NULL,
   IPCSAID              INT                    NOT NULL,
   constraint PK_REL_SCHEME_IP primary key (CSAID)
);

/*==============================================================*/
/* Table: REL_SCHEME_KS                                         */
/*==============================================================*/
CREATE TABLE REL_SCHEME_KS 
(
   CSAID                INT                    NOT NULL AUTO_INCREMENT,
   SCHCSAID             INT                    NOT NULL,
   KSCSAID              INT                    NOT NULL,
   constraint PK_REL_SCHEME_KS primary key (CSAID)
);

/*==============================================================*/
/* Table: INVENTORY_SOFT                                        */
/*==============================================================*/
create table INVENTORY_SOFT 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   SOFTNAME             varchar(100)                   not null,
   GROUPNAME            varchar(20)                    null,
   USERNAME             varchar(20)                    null,
   INSTALLPATH          varchar(200)                   null,
   SOFTPACKAGE          varchar(100)                   not null,
   USERACCOUNT          varchar(100)                   not null,
   CREATETIME           datetime                       not null,
   constraint PK_INVENTORY_SOFT primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci;

/*==============================================================*/
/* Table: INVENTORY_HOST                                        */
/*==============================================================*/
create table INVENTORY_HOST 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   GROUPNAME            varchar(20)                    not null,
   IPADDRESS            varchar(28)                    not null,
   LOGINUSER            varchar(30)                    not null,
   USEAIM               varchar(2000)                  not null,
   constraint PK_INVENTORY_HOST primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci;

/*==============================================================*/
/* Table: INVENTORY_PLAYBOOK                                    */
/*==============================================================*/
create table INVENTORY_PLAYBOOK 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   PBNAME               varchar(100)                   not null,
   RUNSTATUS            varchar(20)                    not null,
   CRONEXPRESSION       varchar(500)                   not null,
   USERACCOUNT          varchar(100)                   not null,
   CREARETIME           datetime                       not null,
   constraint PK_INVENTORY_PLAYBOOK primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci;

/*==============================================================*/
/* Table: INVENTORY_SHELL                                       */
/*==============================================================*/
create table INVENTORY_SHELL 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   FILENAME             varchar(100)                   not null,
   USERACCOUNT          varchar(100)                   not null,
   FILESIZE             int                            not null,
   CREATETIME           datetime                       not null,
   FILETYPE             varchar(100)                   not null,
   UPDATETIME           timestamp                      not null,
   constraint PK_INVENTORY_SHELL primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci;


/*==============================================================*/
/* Table: LOG_SOFT                                              */
/*==============================================================*/
create table LOG_SOFT 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   USERACCOUNT          varchar(100)                   not null,
   CREATETIME           datetime                       not null,
   USERIP               varchar(15)                    not null,
   NOTES                varchar(500)                   not null,
   SOFTCSAID            int                            not null,
   constraint PK_LOG_SOFT primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci;


/*==============================================================*/
/* Table: LOG_PLAYBOOK                                          */
/*==============================================================*/
create table LOG_PLAYBOOK 
(
   CSAID INT NOT NULL AUTO_INCREMENT,
   USERACCOUNT          varchar(100)                   not null,
   CREATETIME           datetime                       not null,
   USERIP               varchar(15)                    not null,
   NOTES                varchar(500)                   not null,
   PBCSAID              int                            not null,
   constraint PK_LOG_PLAYBOOK primary key (CSAID)
)CHARSET=utf8 COLLATE=utf8_general_ci;

/*==============================================================*/
/* Table: TASK_INSTALL_SOFT                                     */
/*==============================================================*/
create table TASK_INSTALL_SOFT
(
   TID INT NOT NULL AUTO_INCREMENT,
   TASKSTATUS           int                            null,
   TASKMSG              varchar(2000)                  null,
   MEDIACSAID           int                            not null,
   TARGETCSAID          int                            not null,
   NOTICESTATUS         int                            not null,
   CREATETIME           datetime                       not null,
   WAREHOUSESERVER      int                            not null,
   constraint PK_TASK_DELETE primary key (TID)
)CHARSET=utf8 COLLATE=utf8_general_ci;

/*==============================================================*/
/* Table: CSA_USER                                              */
/*==============================================================*/
CREATE TABLE CSA_USER
(
 ID INT NOT NULL AUTO_INCREMENT,
 USERNAME VARCHAR(100) NOT NULL,
 PASSWORD VARCHAR(100) NOT NULL,
 PRIMARY KEY (`ID`) 
)CHARSET=utf8 COLLATE=utf8_general_ci;
/*==============================================================*/
/* Table: INVENTORY_PACKAGEGROUP                                */
/*==============================================================*/
CREATE TABLE INVENTORY_PACKAGEGROUP(
  CSAID INT NOT NULL AUTO_INCREMENT,
  GROUPNAME VARCHAR (100) NOT NULL,
  CONSTRAINT PK_inventory_packagegroup PRIMARY KEY (CSAID)
) CHARSET = utf8 COLLATE = utf8_general_ci ;
/*==============================================================*/
/* Table: INVENTORY_PACKAGE                                     */
/*==============================================================*/
CREATE TABLE INVENTORY_PACKAGE(
  CSAID INT NOT NULL AUTO_INCREMENT,
  PACKAGENAME VARCHAR (500) NOT NULL,
  GROUPCSAID INT NOT NULL,
  PACKAGEFULLNAME VARCHAR (500) NOT NULL,
  CONSTRAINT PK_INVENTORY_PACKAGE PRIMARY KEY (CSAID)
) CHARSET = utf8 COLLATE = utf8_general_ci ;
/*==============================================================*/
/* Table: log_ks                                                */
/*==============================================================*/
CREATE TABLE log_ks (
  CSAID INT(11) NOT NULL AUTO_INCREMENT,
  USERACCOUNT VARCHAR(100) NOT NULL,
  UPDATETIME DATETIME NOT NULL,
  USERIP VARCHAR(15) DEFAULT NULL,
  NOTES VARCHAR(500) NOT NULL,
  KICKSTARTCSAID INT(11) NOT NULL,
  PRIMARY KEY (`CSAID`)
)CHARSET = utf8 COLLATE = utf8_general_ci ;