import ConfigParser


class Config:
    def __init__(self):
        cf = self.__getConfigFile()
        self.web_ip = cf.get("web", "ip")
        self.web_port = cf.get("web", "port")
        self.web_pro = cf.get("web", "webProject")
        self.regist_action = cf.get("web", "registAction")
        self.cobbler_ip = cf.get("cobbler", "ip")
        self.cobbler_port = cf.get("cobbler", "port")
        self.ks_path = cf.get("cobbler", "ksPath")
        self.ks_file = cf.get("cobbler", "ksTemplateName")
        self.python_version = cf.get("python", "version")
        self.pro_path = cf.get("log", "proPath")
        self.log_path = cf.get("log", "logPath")
        self.os_path = cf.get("ospath", "path")
        self.cobbler_user = cf.get("cobbler", "user")
        self.cobbler_pwd = cf.get("cobbler", "password")
        self.sync_ksInt = cf.get("cobbler", "synKstIntval")
        self.import_action = cf.get("web","importMsgAction")
        self.sync_kicstarts_action = cf.get('web','syncKicstartsAction')

    def __getConfigFile(self):
        cf = ConfigParser.ConfigParser()
        cf.read("/usr/lib/python2.6/site-packages/cobbler/advanceModel/config/config.init")
        return cf

    def getWebIP(self):
        return self.web_ip

    def getWebPort(self):
        return self.web_port

    def getCobblerIp(self):
        return self.cobbler_ip

    def getCobblerPort(self):
        return self.cobbler_port

    def getWebProJectName(self):
        return self.web_pro

    def getPythonVersion(self):
        return self.python_version

    def getLogPath(self):
        return self.log_path

    def getProPath(self):
        return self.pro_path

    def getOsPath(self):
        return self.os_path

    def getCobblerUsr(self):
        return self.cobbler_user

    def getCobblerPwd(self):
        return self.cobbler_pwd

    def getKsPath(self):
        return self.ks_path

    def getKsTemplateFile(self):
        return self.ks_file

    def getSyncKsInterval(self):
        return self.sync_ksInt

    def getRegistAction(self):
        return self.regist_action

    def getImportMsgAction(self):
        return self.import_action

    def getSyncKicstartsAction(self):
        return self.sync_kicstarts_action