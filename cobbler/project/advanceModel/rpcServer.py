import InitConfig as config
import xmlrpclib

a = config.Config()
ip = a.getCobblerIp()
port = a.getCobblerPort()
cobbler_usr = a.getCobblerUsr()
cobbler_pwd = a.getCobblerPwd()
server = xmlrpclib.Server("http://%s/cobbler_api"%(ip))


def getCobblerService():
    token = server.login(cobbler_usr,cobbler_pwd)
    return server,token
