package com.ultrapower.ultracsa.product.conf;

import java.io.File;

import org.apache.log4j.Logger;

/**
 * 
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 定义工程的路径
 */
public class ProjectPathCfg {
	private static Logger log = Logger.getLogger(ProjectPathCfg.class);


	/**
	 * 获取工程配置文件的目录
	 * @return
	 */
	public static String getProjectCFPath() {
		String path=null;
		try {
			path=ProjectPathCfg.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			if(path.indexOf("bin")>0)
				path=path.substring(0,path.indexOf("bin"));
			else if(path.indexOf("WEB-INF")>0)
				path=path.substring(0,path.indexOf("webapps"));
			else if(path.indexOf("CSAServer")>0)
				path=path.substring(0,path.indexOf("CSAServer"))+"CSAServer"+File.separator;
			path+="webapps"+File.separator+"csa"+File.separator+"WEB-INF"+File.separator+"classes"+File.separator+"conf";
		} catch (Exception e) {
			log.error("",e);
		}
		return path;
	}
	
	/**
	 * 获取工程动态载入Class的路径
	 * @return
	 */
	public static String getDynamicPath(){
		String path=null;
		try {
			path=ProjectPathCfg.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			if(path.indexOf("bin")>0)
				path=path.substring(0,path.indexOf("/bin"));
			else if(path.indexOf("WEB-INF")>0)
				path=path.substring(0,path.indexOf("/webapps"));
			else if(path.indexOf("CSAServer")>0)
				path=path.substring(0,path.indexOf("CSAServer"))+"CSAServer"+File.separator;
			path+="webapps"+File.separator+"csa"+File.separator+"WEB-INF"+File.separator+"classes"+File.separator+"dynamic";
		} catch (Exception e) {
			log.error("",e);
		}
		return path;
	}
	
	/**
	 * 获取工程根目录
	 * @return
	 */
	public static String getProjectHomePath(){
		String path=null;
		try {
			path=ProjectPathCfg.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			if(path.indexOf("bin")>0)
				path=path.substring(0,path.indexOf("/bin"));
			else if(path.indexOf("WEB-INF")>0)
				path=path.substring(0,path.indexOf("/webapps"));
			else if(path.indexOf("CSAServer")>0)
				path=path.substring(0,path.indexOf("CSAServer"))+"CSAServer";
		} catch (Exception e) {
			log.error("",e);
		}
		return path;
	}
	
	/**
	 * 获取工程中file文件夹路径
	 * @return
	 */
	public static String getProjectFilePath(){
		String path=null;
		try {
			path=ProjectPathCfg.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			if(path.indexOf("bin")>0)
				path=path.substring(0,path.indexOf("/bin"));
			else if(path.indexOf("WEB-INF")>0)
				path=path.substring(0,path.indexOf("/webapps"));
			else if(path.indexOf("CSAServer")>0)
				path=path.substring(0,path.indexOf("CSAServer"))+"CSAServer";
			path+=File.separator+"webapps"+File.separator+"csa"+File.separator+"WEB-INF"+File.separator+"file";
		} catch (Exception e) {
			log.error("",e);
		}
		return path;
	}
}
