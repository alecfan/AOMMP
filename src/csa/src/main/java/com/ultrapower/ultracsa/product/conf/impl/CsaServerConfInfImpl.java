/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.conf.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.ultrapower.ultracsa.product.conf.ConfigFile;
import com.ultrapower.ultracsa.product.conf.ProjectConfigServer;
import com.ultrapower.ultracsa.product.conf.ProjectPathCfg;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 *加载CSA工程主配置文件中的信息
 */
public class CsaServerConfInfImpl implements ConfigFile {
	Logger log=Logger.getLogger(CsaServerConfInfImpl.class);
	private static Map<String ,String> fileNameMap=new HashMap();
    private static Map<String,Long> lastUpdateTimeMap=new HashMap();
    
	public CsaServerConfInfImpl() {
		
	}

	/* (non-Javadoc)
	 * @see com.ultrapower.ultracsa.product.conf.ConfigFile#isModified()
	 */
	@Override
	public boolean isModified(String fileName) {
		String filePath=fileNameMap.get(fileName);
		String filename_temp=ProjectPathCfg.getProjectCFPath()+File.separator+"domain"+File.separator+fileName;
		File file=new File(filename_temp);
		if(!file.exists())
			filename_temp=ProjectPathCfg.getProjectCFPath()+File.separator+fileName;
		if(!filename_temp.equals(filePath)){
			fileNameMap.put(fileName,filename_temp);
			return true;
		}else{
			Long lastUpdateTime=lastUpdateTimeMap.get(fileName);
			return lastUpdateTime==null || lastUpdateTime.longValue()==0 || lastUpdateTime.longValue()!=new File(filename_temp).lastModified();
		}
	}

	/* (non-Javadoc)
	 * @see com.ultrapower.ultracsa.product.conf.ConfigFile#reLoad()
	*/
	@Override
	public boolean reLoad(String fileName) {
		String filePath=fileNameMap.get(fileName);
		Properties pro=new Properties();
		try {
			pro.load(new InputStreamReader(new FileInputStream(filePath),"UTF-8"));
			String module=(String)pro.remove("module");
			ProjectConfigServer.putConfig(module, pro);			
			long lastUpdateTime=new File(filePath).lastModified();
			lastUpdateTimeMap.put(fileName, new Long(lastUpdateTime));
		}catch (IOException e) {
			log.error("", e);
		}
		return false;
	}

}
