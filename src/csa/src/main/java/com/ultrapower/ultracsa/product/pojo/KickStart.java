package com.ultrapower.ultracsa.product.pojo;

import java.util.Calendar;
import java.util.Date;

/**
 * 
 * 作者: GuoPengFei
 * 
 * 日期: 2016 4 15
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO Kisckstart实体类
 */
public class KickStart {

	// CSAID INT NOT NULL AUTO_INCREMENT,
	// KSNAME varchar(100) not null,
	// ISOCSAID int not null,
	// USERACCOUNT varchar(100) not null,
	// CREATETIME datetime not null,
	//

	private Integer csaId;
	private String ksName;
	private Iso iso;  //关联iso镜像
	private String userAccount;
	private Date createTime;
	private Date updateTime;

	public Integer getCsaId() {
		return csaId;
	}

	public void setCsaId(Integer csaId) {
		this.csaId = csaId;
	}

	public String getKsName() {
		return ksName;
	}

	public void setKsName(String ksName) {
		this.ksName = ksName;
	}

	public Iso getIso() {
		return iso;
	}

	public void setIso(Iso iso) {
		this.iso = iso;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		Calendar cal=Calendar.getInstance();
		cal.setTime(createTime);
		cal.set(Calendar.MILLISECOND, 0);
		createTime=cal.getTime();
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
