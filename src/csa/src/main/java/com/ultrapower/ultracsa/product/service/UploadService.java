/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.MultipartFile;

import com.ultrapower.ultracsa.product.pojo.Iso;
import com.ultrapower.ultracsa.product.pojo.Server;
import com.ultrapower.ultracsa.product.pojo.Task;

/**
 * 作者: GuoDong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 
 */
public interface UploadService {
	public List<Server> findServer();
	public void insertIso(Iso iso);
	public void updateIso(Iso iso);
	public List<Task> findTaskById(Integer id);
	public List<Iso> findIso();
	public List<String> findIsoType();
	public void up(MultipartFile file,HttpServletRequest request,HttpServletResponse response);
	public void deleteIso(String[] ids,HttpServletRequest request);
	public Iso findIsoById(Integer id);
	public void insertFile(String fileName ,HttpSession  httpSession);
}
