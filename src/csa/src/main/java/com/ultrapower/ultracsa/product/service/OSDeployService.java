/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.service;

import java.util.List;
import java.util.Map;

import javax.print.attribute.standard.MediaSize.ISO;

import org.springframework.web.servlet.support.RequestContext;

import com.ultrapower.ultracsa.product.pojo.InstallPackage;
import com.ultrapower.ultracsa.product.pojo.Iso;
import com.ultrapower.ultracsa.product.pojo.KickStart;
import com.ultrapower.ultracsa.product.pojo.PackageGroup;

/**
 * 作者: GuoPengFei
 * 
 * 日期: 2016 4 25
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 源码说明
 */
public interface OSDeployService {
	public List<KickStart> kickStartList();
	public void insertKickStart(KickStart kickStart);
	public void updateKickStart(KickStart kickStart);
	public Map<String, Object> delteKickStarts(Map<String, String> map);
	public List<String> findIsoList();
	public Map<String, Object> packagesList();
//	public Map<String, Object> findPackagesById(Integer groupCsaid);
	public Map<String, Object> findAllShell();
	public Map<String, Object> editKickstart(Map<String, Object> map,String userName,RequestContext requestContext);
	public Map<String, Object> getKsFile(Map<String, String> map,RequestContext requestContext);
	
	public Map<String, Object> copyKickstart(Map<String, String> map,RequestContext requestContext);
	
	public Map<String, Object> KsList(String ip_search, int currentPage,
			int defaultrow);
	
//	public List<TaskUpload> findTaskById(Integer id);
}
