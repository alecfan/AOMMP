package com.ultrapower.ultracsa.product.pojo;

/**
 * 
 * 作者: GuoPengFei
 * 
 * 日期: 2016 4 27
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 源码说明
 */
public class InstallPackage {

	private Integer csaId; // 主键
	private String packageName;// 安装包名称
	private Integer groupCsaid; // 组id
	private String packageFullName; // 安装包英文全名

	public Integer getCsaId() {
		return csaId;
	}

	public void setCsaId(Integer csaId) {
		this.csaId = csaId;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public Integer getGroupCsaid() {
		return groupCsaid;
	}

	public void setGroupCsaid(Integer groupCsaid) {
		this.groupCsaid = groupCsaid;
	}

	public String getPackageFullName() {
		return packageFullName;
	}

	public void setPackageFullName(String packageFullName) {
		this.packageFullName = packageFullName;
	}

}
