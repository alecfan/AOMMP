/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.ultrapower.ultracsa.product.pojo.IP;
import com.ultrapower.ultracsa.product.pojo.InstallProgram;
import com.ultrapower.ultracsa.product.pojo.KickStart;

/**
 * 作者: guodong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 安装方案服务层
 */
public interface InstallProgramService {

	public List<InstallProgram> getInstallPlanByStatu(Integer status);
	public List<IP> getIpGroup(Integer csaid);
	public Integer insertScheme(InstallProgram installProgram,String[] ipGroup,HttpSession httpSession);
	public void updateScheme(InstallProgram installProgram,String[] ipGroup);
	public InstallProgram getInstallPlanById(Integer id);
	public List<IP> getIpByScheme(Integer id);
	public void updateSchemeStatus(Integer[] idArr,Integer status);
}
