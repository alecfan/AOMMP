/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.module;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ultrapower.ultracsa.product.pojo.Task;
import com.ultrapower.ultracsa.product.util.thread.ThreadPool;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 用于对Task队列进行处理的工具类
 */
public class TaskOperator extends Thread {
	private static Logger log=Logger.getLogger(TaskOperator.class);

	private LinkedList<Object[]> taskList=new LinkedList();
	private LinkedList<Object[]> errorTaskList=new LinkedList();
	
	private static TaskOperator operator=null;
	
	private  TaskOperator(){}
	
	/**
	 * 确保队列为单例模式
	 * @return
	 */
	public synchronized static TaskOperator getInstance(){
		if(operator==null){
			operator=new TaskOperator();
			operator.start();
		}
		return operator;
	}
	
	 public void run(){
         try
         {
             while(true){
                 Object[] taskObj=null;
                 synchronized(taskList){
                     if(taskList.size()>0)
                    	 taskObj=taskList.removeLast();
                     else
                    	 taskList.wait();
                 }
                 if(taskObj!=null){
                	 doTask(taskObj);
                 }
                 sleep(2);
             }
         }
         catch(InterruptedException e)
         {
             log.error("",e);
         }
     }
	 
	 public void addTask( Task task,HandleHook hook){
		
		 if(hook==null)
			 hook=new ISOTaskHook();
	        synchronized(taskList){
	        	taskList.add(new Object[]{task,hook});
	        	taskList.notifyAll();
	        }
	 }
	 
	 /**
	  * 当ISO镜像库被删除时，相关的Task也应该终止执行
	  * @param serverID
	  */
	 public void removeTask(int serverID){
		 synchronized(taskList){
			 for(int i=taskList.size()-1;i>=0;i--){
				 if(((Task)taskList.get(i)[0]).getWarehouseServer()==serverID)
					 taskList.remove(i);
			 }
	     }
		 
		 synchronized(errorTaskList){
			 for(int i=errorTaskList.size()-1;i>=0;i--){
				 if(((Task)errorTaskList.get(i)[0]).getWarehouseServer()==serverID)
					 errorTaskList.remove(i);
			 }
	     }
	 }
	 
	 public void errorTask(Task task,HandleHook hook){
		 synchronized(errorTaskList){
			 errorTaskList.add(new Object[]{task,hook});
			 errorTaskList.notifyAll();
	     }
	 }
	 
	 public void redoTask(Task task,HandleHook hook){
		 synchronized(taskList){
			 taskList.add(new Object[]{task,hook});
	        	taskList.notifyAll();
	     }
	 }
	 
	 /**
	  * 当ISO镜像库恢复连接时，相关的Task也应该恢复执行
	  * @param serverID
	  */
	 public void recoverTask(int serverID){
		 List<Object[]> list=new ArrayList();
		 synchronized(errorTaskList){
			 for(int i=errorTaskList.size()-1;i>=0;i--){
				 if(((Task)errorTaskList.get(i)[0]).getWarehouseServer()==serverID)
					 list.add(errorTaskList.remove(i));
			 }
	     }
		 for(Object[] obj:list){
			 addTask((Task)obj[0],(HandleHook)obj[1]);
		 }
	 }
     
     private void doTask(Object[] task){
    	 try {
    		 HandleHook hook=(HandleHook)task[1];
    		 hook.doWork(((Task)task[0]).getTid());
    		 ThreadPool.execTask("ISO_TASK", (Runnable)hook);
		} catch (Exception e) {
			log.error("",e);
			errorTaskList.add(task);
		}
    	
     }
}
