/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.pojo;

import java.util.Date;

/**
 * 作者: GuoDong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 源码说明
 */
public class IsoLog {

	private Integer csaId;
	private String userAccount;
	private String userIp;
	private String notes;
	private Date updateTime;
	private Integer isoCsaId;
	
	public Integer getCsaId() {
		return csaId;
	}
	public void setCsaId(Integer csaId) {
		this.csaId = csaId;
	}
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	public String getUserIp() {
		return userIp;
	}
	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getIsoCsaId() {
		return isoCsaId;
	}
	public void setIsoCsaId(Integer isoCsaId) {
		this.isoCsaId = isoCsaId;
	}

}
