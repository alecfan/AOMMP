/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.RequestContext;

import com.ultrapower.ultracsa.product.dao.UploadDao;
import com.ultrapower.ultracsa.product.module.TaskOperator;
import com.ultrapower.ultracsa.product.pojo.Iso;
import com.ultrapower.ultracsa.product.pojo.Server;
import com.ultrapower.ultracsa.product.pojo.Task;
import com.ultrapower.ultracsa.product.service.UploadService;
import com.ultrapower.ultracsa.product.util.DateUtil;
import com.ultrapower.ultracsa.product.util.uploadBase;

/**
 * 作者: GuoDong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 源码说明
 */
@Service("uploadService")
@Transactional
public class UploadInfImpl extends uploadBase implements UploadService{
	
	@Resource
    private UploadDao uploadDao;
	
	@Override
	public List<Server> findServer() {
		return this.uploadDao.findServer(new Integer(0));
	}

	@Override
	public void insertIso(Iso iso) {
		uploadDao.insertIso(iso); 
	}

	@Override
	public void updateIso(Iso iso) {
		uploadDao.updateIso(iso);
		Task task = new Task();
		task.setTaskStatus(0);
		task.setTaskType(1);
		task.setNoticeStatus(0);
		task.setCreateTime(new Date());
		List<Server> serverList = uploadDao.findServer(null);
		for(Server server:serverList){
			task.setMediaCsaId(iso.getCsaId());
			task.setWarehouseServer(server.getCsaId());
			uploadDao.insertIsoTask(task);
			TaskOperator.getInstance().addTask(task, null);
		}
	}

	/* (non-Javadoc)
	 * 获取镜像上传任务日志
	 */
	@Override
	public List<Task> findTaskById(Integer id) {
		// TODO Auto-generated method stub
		return uploadDao.findTaskById(id);
	}

	/* (non-Javadoc)
	 * @see com.ultrapower.ultracsa.product.service.UploadService#findIso()
	 */
	@Override
	public List<Iso> findIso() {
		// TODO Auto-generated method stub
		return uploadDao.findIso();
	}

	/* (non-Javadoc)
	 * @see com.ultrapower.ultracsa.product.service.UploadService#findIsoType()
	 */
	@Override
	public List<String> findIsoType() {
		List<Iso> isoList = new ArrayList<Iso>();
		isoList = uploadDao.findIsoType();
		List<String> typeList = new ArrayList<String>();
		for(Iso i:isoList){
			if(i!=null){
				typeList.add(i.getIsoType());
			}else{
				typeList.add("Unknown");
			}
		}
		return typeList;
	}

	/* 上传iso镜像到服务器
	 * @see com.ultrapower.ultracsa.product.service.UploadService#up(org.springframework.web.multipart.MultipartFile, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void up(MultipartFile file, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String path = request.getSession().getServletContext().getRealPath("")+"\\WEB-INF\\file\\iso\\";
			super.upload(file,path,request);
			response.getWriter().print(super.getFileName());
			String fileName = file.getOriginalFilename().substring(0,
					file.getOriginalFilename().lastIndexOf("."));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private List<Task> deleteIsoFromDB(String[] ids,HttpServletRequest request) {
		List<Task> result=new ArrayList();
		Map<String, Object> param=new HashMap<String, Object>();
		List<Server> serverList = uploadDao.findServer(null);
	    RequestContext requestContext = new RequestContext(request);
	    Task task = new Task();
	    task.setTaskStatus(0);
		task.setTaskType(2);
		task.setNoticeStatus(0);
		task.setCreateTime(new Date());
		for(Server server:serverList){
			for(String id:ids){
				param.put("mediaCsaId", id);
				param.put("warehouseServer", server.getCsaId());
				task.setMediaCsaId(new Integer(id));
				task.setWarehouseServer(server.getCsaId());
				uploadDao.insertDeleteIsoTask(task);
				result.add(task);
			}
		}
		List<Map> list = new ArrayList<Map>();
		for(String id:ids){
			param=new HashMap<String, Object>();
			param.put("id", id);
			param.put("statusDescr", requestContext.getMessage("Deleting"));
			list.add(param);
		}
		uploadDao.deleteIso(list);
		return result;
	}
	
	@Override
	public void deleteIso(String[] ids,HttpServletRequest request) {
		List<Task> list=deleteIsoFromDB(ids,request);
		for(Task task:list){
			TaskOperator.getInstance().addTask(task, null);
		}
	}

	@Override
	public Iso findIsoById(Integer id) {
		return uploadDao.findIsoById(id);
	}

	@Override
	public void insertFile(String fileName ,HttpSession  httpSession) {
		Iso iso = new Iso();
		iso.setUserAccount(httpSession.getAttribute("userName")+"");
		iso.setIsoName(fileName.replace(".iso", ""));
		iso.setIsoType("未分类");
		iso.setStatusBit("0");
		iso.setStatusDescr("不可用");
		uploadDao.insertFile(iso);
	}

}
