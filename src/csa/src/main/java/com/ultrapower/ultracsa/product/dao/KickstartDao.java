package com.ultrapower.ultracsa.product.dao;

import java.util.List;
import java.util.Map;

import com.ultrapower.ultracsa.product.pojo.InstallPackage;
import com.ultrapower.ultracsa.product.pojo.KickStart;
import com.ultrapower.ultracsa.product.pojo.KickstartLog;
import com.ultrapower.ultracsa.product.pojo.PackageGroup;
import com.ultrapower.ultracsa.product.pojo.ShellFile;

public interface KickstartDao {
	/**
	 * 查询kicstart数据
	 * 
	 * @return
	 */
	public List<KickStart> kickStartList();

	/**
	 * 新增kiscstart
	 * 
	 * @param kickStart
	 */
	public void insertKickStart(KickStart kickStart);

	/**
	 * 修改kickstart
	 * 
	 * @param kickStart
	 */
	public void updateKickStart(KickStart kickStart);

	/**
	 * 查询所有的预安装包组
	 * 
	 * @return
	 */
	public List<PackageGroup> packageGroupList();

	/**
	 * 根据id查询kickstart
	 * 
	 * @param csaId
	 * @return
	 */
	public KickStart findKickStartById(Integer csaId);

	/**
	 * 根据id查询package
	 * 
	 * @param fullName
	 * @return
	 */
	public InstallPackage findKickStartPacById(Integer csaId);

	/**
	 * 批量删除kickstart
	 * 
	 * @param list
	 * @return
	 */
	public int batchDeleteKickstarts(List<Integer> list);

	/**
	 * 根据id查询预安装包
	 * 
	 * @param groupCsaid
	 * @return
	 */
	public List<InstallPackage> findPackagesById(Integer groupCsaid);

	/**
	 * 根据ks名称搜索查询kickstart
	 * 
	 * @param ks_search
	 * @return
	 */
	public List<KickStart> KsList(String ks_search);

	/**
	 * 批量新增ks和shell中间表信息
	 * @param tempList
	 */
	public void insertKickStartShell(List<Map<String, Integer>> tempList);

	/**
	 * 批量删除ks和shell中间表
	 * @param csaId
	 */
	public void deleteShellKs(Integer csaId);

	/**
	 * 拷贝ks和shell
	 * @param csaId
	 */
	public void copytKickStartShell(Integer newCsaId ,Integer oldCsaId);
	/**
	 * 批量删除ks 和shell中间表数据
	 * @param asList
	 */
	public void batchDeleteKicAndShell(List<Integer> asList);

	/**
	 * 根据kickstart id 查询所有的脚本
	 * @param kic
	 * @return
	 */
	public List<ShellFile> getShellByKicstartId(Integer kic);

	/**
	 * 插入ks日志
	 * @param kickstartLog
	 */
	public void insertKsLog(KickstartLog kickstartLog);

	/**
	 * 批量删除ks日志
	 * @param asList
	 */
	public void batchDeleteKicLog(List<Integer> asList);
}
