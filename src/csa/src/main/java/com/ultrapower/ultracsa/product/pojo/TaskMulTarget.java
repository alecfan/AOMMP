/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.pojo;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 用于描述软件安装过程的任务
 */
public class TaskMulTarget extends Task {
	/*目标主机的ID*/
	private Integer targetCsaId;
	
	public Integer getTargetCsaId() {
		return targetCsaId;
	}

	public void setTargetCsaId(Integer targetCsaId) {
		this.targetCsaId = targetCsaId;
	}

	/**
	 * 
	 */
	public TaskMulTarget() {
		// TODO Auto-generated constructor stub
	}

}
