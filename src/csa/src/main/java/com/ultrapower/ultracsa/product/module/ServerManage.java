/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.module;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;

import com.ultrapower.ultracsa.product.conf.ProjectConfigServer;
import com.ultrapower.ultracsa.product.util.dynamic.DynamicUpdateServer;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 基础服务
 */
public class ServerManage  extends HttpServlet {
	private static Logger log=Logger.getLogger(ServerManage.class);
    private static List<ModuleServer> list=new ArrayList();
	
	/**
	 * 
	 */
	public ServerManage() {
		// TODO Auto-generated constructor stub
	}
	
	public void init() throws ServletException {
		 startServer();
	}
	
	public boolean startServer(){
		try{
		Collection taskList=ProjectConfigServer.getConfig("task").values();
		list.clear();
		for (Iterator iterator = taskList.iterator(); iterator.hasNext();) {
			String className = (String) iterator.next();
			
			try {
				ModuleServer ms=(ModuleServer)DynamicUpdateServer.getInstance().getNewClass(className).newInstance();
				ms.startServer();
				list.add(ms);
			} catch (Exception e) {
				log.error("",e);
				return false;
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return true;
	}
	
	public void addObject(Object obj){
		for (ModuleServer ms : list) {
			ms.addObject(obj);
		}
	}

}
