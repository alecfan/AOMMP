/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.conf;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 同统一提供配置文件访问服务
 */
public class ProjectConfigServer {
	/*存放所有配置文件的配置项*/
	private static Map<String,Map<String,String>> cfMap=new HashMap();
	
	/**
	 * 按配置文件的用途类型向系统中注册配置文件中的配置信息
	 * @param module 配置文件的用途类型
	 * @param proes  配置文件中的配置信息
	 */
	public  static void putConfig(String module,Map proes){
		cfMap.put(module, proes);
	}
	
	/**
	 * 查询配置文件中的配置信息
	 * @param module 文件的用途类型
	 * @param key 配置文件条目  
	 * @return  
	 */
	public static String getConfig(String module,String key){
		if(cfMap.containsKey(module))
			return cfMap.get(module).get(key);
		else
			return null;
	}

	/**
	 * 查询配置文件中的配置信息
	 * @param module 文件的用途类型
	 * @return  
	 */
	public static Map<String,String> getConfig(String module){
		if(cfMap.containsKey(module))
			return cfMap.get(module);
		else
			return null;
	}
}
