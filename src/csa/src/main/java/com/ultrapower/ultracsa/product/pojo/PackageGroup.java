package com.ultrapower.ultracsa.product.pojo;

/**
 * 
 * 作者: huipu
 * 
 * 日期: 2016 4 27
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 源码说明
 */
public class PackageGroup {
	private Integer csaId; // 主键
	private String groupName; // 组名

	public Integer getCsaId() {
		return csaId;
	}

	public void setCsaId(Integer csaId) {
		this.csaId = csaId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
