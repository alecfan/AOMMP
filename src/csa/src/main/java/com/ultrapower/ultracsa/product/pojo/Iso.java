/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.pojo;

import java.util.Date;

/**
 * 作者: GuoDong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 镜像实体
 */
public class Iso {

	private Integer csaId;
	private String isoName;
	private String isoType;
	private String isoVersion;
	private String isoUserVersion;
	private String statusBit;
	private String statusDescr;
	private Integer isoBit;
	private String userAccount;
	private Date createTime;
	private String useAim;
	
	public Integer getCsaId() {
		return csaId;
	}
	public void setCsaId(Integer csaId) {
		this.csaId = csaId;
	}
	public String getIsoName() {
		return isoName;
	}
	public void setIsoName(String isoName) {
		this.isoName = isoName;
	}
	public String getIsoType() {
		return isoType;
	}
	public void setIsoType(String isoType) {
		this.isoType = isoType;
	}
	public String getIsoVersion() {
		return isoVersion;
	}
	public void setIsoVersion(String isoVersion) {
		this.isoVersion = isoVersion;
	}
	public String getIsoUserVersion() {
		return isoUserVersion;
	}
	public void setIsoUserVersion(String isoUserVersion) {
		this.isoUserVersion = isoUserVersion;
	}
	public Integer getIsoBit() {
		return isoBit;
	}
	public void setIsoBit(Integer isoBit) {
		this.isoBit = isoBit;
	}
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUseAim() {
		return useAim;
	}
	public void setUseAim(String useAim) {
		this.useAim = useAim;
	}
	public String getStatusBit() {
		return statusBit;
	}
	public void setStatusBit(String statusBit) {
		this.statusBit = statusBit;
	}
	public String getStatusDescr() {
		return statusDescr;
	}
	public void setStatusDescr(String statusDescr) {
		this.statusDescr = statusDescr;
	}
}
