/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.RequestContext;

import com.ultrapower.ultracsa.product.pojo.Iso;
import com.ultrapower.ultracsa.product.pojo.Server;
import com.ultrapower.ultracsa.product.pojo.Task;
import com.ultrapower.ultracsa.product.service.UploadService;
import com.ultrapower.ultracsa.product.util.uploadBase;

/**
 * 作者: GuoDong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 源码说明
 */
@Controller
public class UploadController{
	private static Logger logger = Logger.getLogger(UploadController.class);  

	@Resource
	private UploadService uploadService;
	
	@RequestMapping(value = "/uploadServerList", method = RequestMethod.GET)
	@ResponseBody  
	public Map<String, Object> findServer(Model model){
		List<Server> serverList = new ArrayList<Server>();
		serverList = (List<Server>) uploadService.findServer();
		model.addAttribute("serverList", serverList);
		Map<String, Object> modelMap = new HashMap<String, Object>(3);  
	    modelMap.put("total", serverList.size());  
	    modelMap.put("data", serverList);  
	    modelMap.put("success", "true");
		return modelMap;
	}
	
	@RequestMapping(value="/uploadAddIso",method=RequestMethod.POST)
	public String insertIso(Iso iso,Model model){
		uploadService.insertIso(iso);
		return"upload";
	}

	@RequestMapping(value="/uploadEditIso",method=RequestMethod.POST)
	public String updateIso(Iso iso,Model model){
		uploadService.updateIso(iso);
		return"upload";
	}
	
	@RequestMapping(value = "/uploadTaskList", method = RequestMethod.GET)
	@ResponseBody  
	public Map<String, Object> findTaskById(Integer taskId,Model model){
		List<Task> taskList = new ArrayList<Task>();
		taskList = (List<Task>) uploadService.findTaskById(taskId);
		model.addAttribute("taskList", taskList);
		Map<String, Object> modelMap = new HashMap<String, Object>(3);  
	    modelMap.put("total", taskList.size());  
	    modelMap.put("data", taskList);  
	    modelMap.put("success", "true");
		return modelMap;
	}
	
	@RequestMapping(value = "/updateServerStatus", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> updateServerStatus(Model model){
		List<Server> serverList = new ArrayList<Server>();
		List<Iso> isoList = new ArrayList<Iso>();
		serverList = (List<Server>) uploadService.findServer();
		isoList = (List<Iso>) uploadService.findIso();
		Map<String, Object> modelMap = new HashMap<String, Object>();
		modelMap.put("serverList", serverList);
		modelMap.put("isoList", isoList);
		return modelMap;
	}
	@RequestMapping(value = "/uploadIso")
	public String isoLibrary(Server server,Model model){
		List<Server> serverList = new ArrayList<Server>();
		List<Iso> isoList = new ArrayList<Iso>();
		List<String> typeList = new ArrayList<String>();
		serverList = (List<Server>) uploadService.findServer();
		isoList = (List<Iso>) uploadService.findIso();
		typeList = (List<String>) uploadService.findIsoType();
		logger.info(serverList.size());
		model.addAttribute("serverList", serverList);
		model.addAttribute("isoList", isoList);
		model.addAttribute("typeList", typeList);
		return "iso_libraries";
	}
	
	@RequestMapping(value = "/up",method = RequestMethod.POST)
	public void upload(@RequestParam("file")MultipartFile file,HttpServletRequest request,HttpServletResponse response) {
		 uploadService.up(file, request, response);
	}
	
	@RequestMapping(value = "/deleteiso",method = RequestMethod.POST)
	@ResponseBody
    public String deleteIso(@RequestBody String[] ids,HttpServletRequest request) {
         uploadService.deleteIso(ids,request);
         return "true";
    }
	@RequestMapping(value = "/saveIsoInfo",method = RequestMethod.POST)
	@ResponseBody
    public String saveIsoInfo(Iso iso) {
		 uploadService.updateIso(iso);
         return "true";
    }
	@RequestMapping(value = "/findIsoById", method = RequestMethod.POST)
	@ResponseBody
	public Iso findIsoById(Integer isoId,Model model){
		Iso iso = uploadService.findIsoById(isoId);
		return iso;
	}
	
	@RequestMapping(value = "/insertFile",method = RequestMethod.POST)
	@ResponseBody
    public String insertFile(String fileName ,HttpSession  httpSession) {
		 uploadService.insertFile(fileName,httpSession);
         return "true";
    }
}
