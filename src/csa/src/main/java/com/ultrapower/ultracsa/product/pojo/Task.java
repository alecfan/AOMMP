/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.pojo;

import java.util.Date;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 用于映射软件、OS安装及ISO镜像上传、删除任务
 */
public class Task {
	/*镜像上传 */
	public static int OS_UPLOAG=1;
	/*镜像删除 */
	public static int OS_DELETE=2;
	/*OS按装 */
	public static int OS_DEPLOY=3;
	
	/*任务ID*/
	private Integer tid;
	/*Task的类型，1--镜像上传   2--镜像删除   3--OS按装*/
	private Integer taskType;
	/*任务状态*/
	private Integer taskStatus;
	/*任务执行失败时的错误信息*/
	private String taskMsg;
	/*相关介质ID，如软件、ISO镜像等*/
	private Integer mediaCsaId;
	/*相关安装方案的ID*/
	private Integer schemeCsaId;
	/*通知状态：0--未通知；1--已通知；*/
	private Integer noticeStatus;
	/*创建时间*/
	private Date createTime;
	/*任务结束时间*/
	private Date endTime;
	/*相关镜像库或软件库的id*/
	private Integer warehouseServer;

	
	public Integer getTid() {
		return tid;
	}


	public void setTid(Integer tid) {
		this.tid = tid;
	}


	public Integer getTaskStatus() {
		return taskStatus;
	}


	public void setTaskStatus(Integer taskStatus) {
		this.taskStatus = taskStatus;
	}


	public String getTaskMsg() {
		return taskMsg;
	}


	public void setTaskMsg(String taskMsg) {
		this.taskMsg = taskMsg;
	}


	public Integer getMediaCsaId() {
		return mediaCsaId;
	}


	public void setMediaCsaId(Integer mediaCsaId) {
		this.mediaCsaId = mediaCsaId;
	}


	public Integer getNoticeStatus() {
		return noticeStatus;
	}


	public void setNoticeStatus(Integer noticeStatus) {
		this.noticeStatus = noticeStatus;
	}


	public Date getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	public Date getEndTime() {
		return endTime;
	}


	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}


	public Integer getWarehouseServer() {
		return warehouseServer;
	}


	public void setWarehouseServer(Integer warehouseServer) {
		this.warehouseServer = warehouseServer;
	}


	public Integer getTaskType() {
		return taskType;
	}


	public void setTaskType(Integer taskType) {
		this.taskType = taskType;
	}


	public Integer getSchemeCsaId() {
		return schemeCsaId;
	}


	public void setSchemeCsaId(Integer schemeCsaId) {
		this.schemeCsaId = schemeCsaId;
	}


	/**
	 * 
	 */
	public Task() {
		// TODO Auto-generated constructor stub
	}

}
