/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.restful;


import java.util.List;

import com.ultrapower.ultracsa.product.dao.IPDao;
import com.ultrapower.ultracsa.product.pojo.IP;
import com.ultrapower.ultracsa.product.util.BeanUtil;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 源码说明
 */
public class IntallStatus implements Runnable {
	private String ip=null;
	private Integer percent=null;
	private String msg=null;

	public IntallStatus(String ip,String percent,String msg){
		this.ip=ip;
		this.percent=Integer.parseInt(percent);
		this.msg=msg;
	}
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		IP ipObj=BeanUtil.getBean(IPDao.class).getIP(ip);
		if(ipObj!=null){
			ipObj.setIpStatus(msg);
			ipObj.setPercent(percent);
			BeanUtil.getBean(IPDao.class).updateIP(ipObj);
		}
			
	}

}
