package com.ultrapower.ultracsa.product.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.ultrapower.ultracsa.product.pojo.IP;
import com.ultrapower.ultracsa.product.pojo.IPLog;
/**
 * 
 * 作者: GuoPengFei
 * 
 * 日期: 2016 4 20
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 源码说明
 */
public interface IPDao {
	public List<IP> IPList(String ip_search);

	public IP getIP(@Param("ip")String ip);

	public void insertIP(IP ip);

	public void updateIP(IP ip);

	public int batchUpdateIps(List<IP> list);

	// public List<TaskUpload> findTaskById(Integer id);
	public int batchDeleteIps(List<Integer> list);

	public int insertIps(List<IP> list);

	public IP findById(Integer csaId);
	
	/**
	 * 单个新增ip log
	 * @param ipLog
	 */
	public void insertLog(IPLog ipLog);
	
	
	/**
	 * 批量新增ip log
	 * @param list
	 */
	public void insertLogs(List<IPLog> list);

	/**
	 * 批量删除ip log
	 * @param list
	 */
	public void batchDeleteIpLogs(List<Integer> list);
	/**
	 * 查询某条ip对应的所有修改日志
	 * @param ipCsaId
	 * @return
	 */
	public List<IPLog> ipLogList(Integer ipCsaId);
	
	public List<IP> IPListByInstallDetail(Map map);
	
	public List<IP> IPListByScheme(Integer csaid);
}
