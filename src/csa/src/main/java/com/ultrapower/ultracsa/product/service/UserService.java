package com.ultrapower.ultracsa.product.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.support.SessionStatus;

import com.ultrapower.ultracsa.product.pojo.Iso;
import com.ultrapower.ultracsa.product.pojo.User;

public interface UserService {
	public User getUserById(Integer id);
	public List getListByAge(Integer age);
	public void insertUser(User user);
	public String login(User user,SessionStatus sessionStatus,HttpSession httpSession);
}
