/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.ultrapower.ultracsa.product.pojo.IP;
import com.ultrapower.ultracsa.product.pojo.InstallProgram;
import com.ultrapower.ultracsa.product.pojo.KickStart;
import com.ultrapower.ultracsa.product.pojo.Task;

/**
 * 作者: guodong
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 安装方案查询dao
 */
public interface InstallProgramDao {

	public List<InstallProgram> getInstallPlanByStatu(@Param("status")Integer status);
	public List<IP> getIpByScheme(Integer schcsaid);
	public InstallProgram getInstallPlanById(@Param("id")Integer id);
	public List<IP> getIpGroup(@Param("csaid")Integer csaid);
	public void insertScheme(InstallProgram installProgram);
	public void updateScheme(InstallProgram installProgram);
	public void updateInstallInf(InstallProgram installProgram);
	public void insertRelSchemeIp(Map map);
	public void deleteRelSchemeIp(Integer id);
	public void updateSchemeStatus(List list);
	public void deleteScheme(Integer[] idArr);
	public void insertInstallIsoTask(Task task);
	public void deleteRelSchemeIpBatch(Integer[] idArr);
}
