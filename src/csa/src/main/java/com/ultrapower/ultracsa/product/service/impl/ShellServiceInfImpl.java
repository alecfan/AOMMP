/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.service.impl;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import com.ultrapower.ultracsa.product.conf.ProjectPathCfg;
import com.ultrapower.ultracsa.product.dao.ModelDao;
import com.ultrapower.ultracsa.product.pojo.ShellFile;
import com.ultrapower.ultracsa.product.service.ShellService;
import com.ultrapower.ultracsa.product.util.uploadBase;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 源码说明
 */
@Service("ShellService")
public class ShellServiceInfImpl extends uploadBase implements ShellService{
	private static Logger log=Logger.getLogger(ShellServiceInfImpl.class);
	
	@Resource
	private ModelDao modelDao;
	
	@Override
	public void shellList(Model model) {
		List<String> list=null;
	    list=modelDao.getShellType();
		if(list==null)
			list=new ArrayList();
		model.addAttribute("shellTypeList", list);
	}
	
	@Override
	public List<String> getShellTab() {
		List<String> list=null;
	    list=modelDao.getShellType();
		if(list==null)
			list=new ArrayList();
		return list;
	}
	
	@Override
	public List<ShellFile> getShell(ShellFile file){
	    return modelDao.getShell(file);
	}

	@Override
	public List<ShellFile> shellListByType(Model model,
			HttpServletRequest request, HttpServletResponse response) {
		ShellFile file=new ShellFile();
		file.setFileType(request.getParameter("fileType"));
		return (modelDao.getShell(file));
	}

	@Override
	public long uploadShell(MultipartFile file,HttpServletRequest request,HttpServletResponse response) {
		String tomcatPath = ProjectPathCfg.getProjectHomePath()+File.separator+"webapps"+File.separator+"csa"+File.separator+"WEB-INF"+
				File.separator+"file"+File.separator+"script"+File.separator;
		
		try {
			this.setAllowSuffix(".sh,.txt,.yml");
			super.upload(file,tomcatPath,request);
			return file.getSize();
		} catch (Exception e) {
			log.error("",e);
			return -1;
		}
	}

	@Override
	public boolean createShellFile(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			request.setCharacterEncoding("UTF-8");
			String fileNama=request.getParameter("fileNama");
			String fileSize=(String)request.getParameter("fileSize");
			String fileType=request.getParameter("fileType");
			String createUser=(String)request.getSession().getAttribute("userName");
			ShellFile sf=new ShellFile();
			sf.setCreateTime(new Date());
			sf.setFileName(fileNama);
			sf.setFileType(fileType);
			sf.setUserAccount(createUser);
			sf.setFileSize((long)Math.ceil(Long.parseLong(fileSize)/1024.0));
			modelDao.insertShellFile(sf);
			return true;
		} catch (UnsupportedEncodingException e) {
			log.error("",e);
		}
		
		return false;
	}

	@Override
	public boolean deleteShellFile(HttpServletRequest request,
			HttpServletResponse response) {
		String tomcatPath = ProjectPathCfg.getProjectHomePath()+File.separator+"webapps"+File.separator+"csa"+File.separator+"WEB-INF"+
				File.separator+"file"+File.separator+"script"+File.separator;
		
		String ids=request.getParameter("fileids");
		String[] arrID=ids.split(",");
		List<ShellFile> temp=null;
		for (String string : arrID) {
			ShellFile shellFile=new ShellFile();
			shellFile.setCsaID(Integer.parseInt(string));
			temp=modelDao.getShell(shellFile);
			if(temp!=null){

				//数据库中删除后再删除物理文件
				if(modelDao.deleteShellFile(temp.get(0))){
					java.io.File file=new java.io.File(tomcatPath+temp.get(0).getFileName());
					System.out.println(file.delete());
				}
			}
		}
		return true;
	}

	public String getOrigName(String fileName){
		if(fileName.indexOf("/")>0){
			fileName=fileName.substring(fileName.lastIndexOf("/"));
		}else if(fileName.indexOf("\\")>0){
			fileName=fileName.substring(fileName.lastIndexOf("\\"));
		}
		return fileName;
	}
}
