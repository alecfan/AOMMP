/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.conf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;

import com.ultrapower.ultracsa.product.util.security.SecurityService;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * TODO 源码说明
 */
public class ConfFileLoadServer extends HttpServlet{ 
	private static Logger log = Logger.getLogger(ConfFileLoadServer.class);
    private static ConfigFileMonitor moniter=ConfigFileMonitor.getInstance("Monitor Web Configration");
    
	//存量管理WEB应该初始化
	public void init() throws ServletException {
		
		//初始化配置文件
		if (ConfFileLoadServer.initConfEnv()) {
			log.info("Ultra-CSA WEB应用启动完成！");
		} else {
			log.error("Ultra-CSA  WEB应用启动失败！");
		}
		
		//初始化PASM权限点
		if("true".equals(ProjectConfigServer.getConfig("core","pasm_integration"))){
			if(SecurityService.initPasmResource())
				log.info("Ultra-CSA 权限点初始化完成！");
			else
				log.info("Ultra-CSA 权限点初始化失败！");
		}
	}
	
	public static boolean initConfEnv() {
		long startLong = System.currentTimeMillis();
		//定义工程配置文件的文件路径
		String cfpathFile=ProjectPathCfg.getProjectCFPath()+File.separator+"configFile.txt";
		BufferedReader br=null;
		try {
			br=new BufferedReader(new InputStreamReader(new FileInputStream(cfpathFile),"UTF-8"));
			while(true){
				String line=br.readLine();
				if(line==null)
					break;
				String[] arr=line.split(":");
				moniter.add(arr[0],arr[1]);
			}
			moniter.setDaemon(true);
			moniter.start();
			log.info("配置CSA 应用工作环境成功,耗时[" + (System.currentTimeMillis() - startLong) + "]mm.");
		    return true;
		} catch (Exception e) {
			log.error("",e);
			return false;
		}finally{
			if(br!=null)
				try {
					br.close();
				} catch (IOException e) {
					log.error("",e);
				}
		}
	}
	
	
}
