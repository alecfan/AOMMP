/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.module;

import com.ultrapower.ultracsa.product.pojo.Task;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 用于进行Task处理的回调钩子
 */
public interface HandleHook {
	/**
	 * 负责进行Task任务的通知，调用远程接口
	 * @param task
	 * @param additionParam  附加参数，可以用于调用端与钩子进行状态通信，可以为空
	 * @return 通知成功时返回true，否者为false
	 */
	public boolean doWork(Integer taskID) throws ConnectionException;
}
