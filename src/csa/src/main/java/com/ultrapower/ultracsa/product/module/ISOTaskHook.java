package com.ultrapower.ultracsa.product.module;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ultrapower.ultracsa.product.dao.IPDao;
import com.ultrapower.ultracsa.product.dao.InstallProgramDao;
import com.ultrapower.ultracsa.product.dao.KickstartDao;
import com.ultrapower.ultracsa.product.dao.ModelDao;
import com.ultrapower.ultracsa.product.dao.UploadDao;
import com.ultrapower.ultracsa.product.pojo.IP;
import com.ultrapower.ultracsa.product.pojo.InstallProgram;
import com.ultrapower.ultracsa.product.pojo.Iso;
import com.ultrapower.ultracsa.product.pojo.KickStart;
import com.ultrapower.ultracsa.product.pojo.RelSchemeIP;
import com.ultrapower.ultracsa.product.pojo.RelSchemeKS;
import com.ultrapower.ultracsa.product.pojo.Server;
import com.ultrapower.ultracsa.product.pojo.Task;
import com.ultrapower.ultracsa.product.util.BeanUtil;
import com.ultrapower.ultracsa.product.util.HttpClientUtil;

/**
	 * 
	 * 作者: xtxb
	 * 
	 * 日期: 2016
	 * 
	 * 版权说明：北京神州泰岳软件股份有限公司
	 * 
	 * 调用ISO进行库的接口，派发通知
	 */
public	class ISOTaskHook implements HandleHook,Runnable{
		Logger log=Logger.getLogger(ISOTaskHook.class);
		private Integer taskID=null;
		
		@Override
		public boolean doWork(Integer taskID) throws ConnectionException {
			this.taskID=taskID;
			return false;
		}

		@Override
		public void run() {
			ModelDao dao=BeanUtil.getBean(ModelDao.class);
			Task task=dao.getTaskByID(taskID);
			Integer serverID=task.getWarehouseServer();
			Server isoServer=dao.getServerByID(serverID);
			//如果镜像库不存在了，则将Task删除
			if(isoServer==null){
				task.setTaskStatus(2);
				dao.updateTask(task);
				return;
			}
			if(isoServer.getServerStatusMark().equals("1")){
				TaskOperator.getInstance().errorTask(task, this);
				return;
			}
			
			if(task.getTaskType()==Task.OS_UPLOAG){
				//上传镜像
				Iso iso=dao.getISO(task.getMediaCsaId());
				if(iso==null){
					task.setNoticeStatus(1);
					task.setTaskStatus(2);
					dao.updateTask(task);
					return;
				}
				Map<String,String> params=new HashMap();
				params.put("osName", iso.getIsoName());
				params.put("arch", "x86_"+iso.getIsoBit());
				params.put("tid", ""+task.getTid());
				if(!HttpClientUtil.curlPost(isoServer.getServerUrl()+"/upload", params))
					TaskOperator.getInstance().redoTask(task, this);
				else{
					task.setNoticeStatus(1);
					task.setTaskStatus(0);
					dao.updateTask(task);
				}
//				
//				XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
//				try {
//					config.setServerURL(new URL(isoServer.getServerUrl()));
//					XmlRpcClient client = new XmlRpcClient();
//					client.setConfig(config);
//					
//					Iso iso=BeanUtil.getBean(ModelDao.class).getISO(task.getMediaCsaId());
//					Object [] params = new Object[]{iso.getIsoName(),"x86_"+iso.getIsoBit(),task.getTid()};
//					String soInfo = (String) client.execute("builderOS",params);
//					if(soInfo.indexOf("error")<0)
//						BeanUtil.getBean(ModelDao.class).updateTask(task);
//					log.info(soInfo);
//				} catch (Exception e) {
//					log.error("服务器请求失败！",e);
//					TaskOperator.getInstance().redoTask(task, this);
//				}
			}else if(task.getTaskType()==Task.OS_DELETE){
				//删除镜像方法
				Iso iso=dao.getISO(task.getMediaCsaId());
				Map<String,String> params=new HashMap();
				params.put("osName", iso.getIsoName());
				params.put("tid", ""+task.getTid());
				if(!HttpClientUtil.curlPost(isoServer.getServerUrl()+"/remove", params))
					TaskOperator.getInstance().redoTask(task, this);
				else{
					task.setNoticeStatus(1);
					task.setTaskStatus(0);
					dao.updateTask(task);
				}
				
//				XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
//				try {
//					config.setServerURL(new URL(isoServer.getServerUrl()));
//					XmlRpcClient client = new XmlRpcClient();
//					client.setConfig(config);
//					
//					Iso iso=BeanUtil.getBean(ModelDao.class).getISO(task.getMediaCsaId());
//					String isoName=iso.getIsoName();
//					if(isoName.indexOf(".")>0)
//						isoName=isoName.substring(0 , isoName.indexOf("."));
//					Object [] params = new Object[]{isoName,task.getTid()};
//					String soInfo = (String) client.execute("removeOS",params);
//					if(soInfo.indexOf("error")<0)
//						BeanUtil.getBean(ModelDao.class).updateTask(task);
//					log.info(soInfo);
//				} catch (Exception e) {
//					log.error("服务器请求失败！",e);
//					TaskOperator.getInstance().redoTask(task, this);
//				}
			}else if(task.getTaskType()==Task.OS_DEPLOY){
				
				//获取安装方案
				InstallProgram scheme=BeanUtil.getBean(InstallProgramDao.class).getInstallPlanById(task.getSchemeCsaId());
				
				//获取安装部署配置
				RelSchemeKS sks=new RelSchemeKS();
				sks.setSchCsaId(task.getSchemeCsaId());
				List<RelSchemeKS> listks=dao.getRelSchemeKS(sks);
				KickStart ks=null;
				if(listks!=null && listks.size()==1){
					ks=BeanUtil.getBean(KickstartDao.class).findKickStartById(listks.get(0).getKsCsaId());
				}else{
					log.error("没有查询到安装方案关联的部署配置！！！");
					return;
				}
				if(ks==null){
					log.error("安装方案关联的部署配置（CSAID）:"+listks.get(0).getKsCsaId()+"不存在!");
					return;
				}						
			
				//获取相关IP
				RelSchemeIP sip=new RelSchemeIP();
				sip.setSchCsaId(task.getSchemeCsaId());
				List<RelSchemeIP> listip=dao.getRelSchemeIP(sip);
				SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss"); 
				List<Map<String,String>> installMap=new ArrayList<Map<String,String>>();
				if(listip!=null){
					IP ipini=null;
					Map<String,String> map=null;
					for (RelSchemeIP ip : listip) {
						ipini=BeanUtil.getBean(IPDao.class).findById(ip.getIpCsaId());
						if(ipini!=null){
							map=new HashMap();
							map.put("hostName","");
							map.put("systemName",scheme.getUserAccount()+sdf.format(scheme.getCreateTime()));
							map.put("profileName",ks.getUserAccount()+sdf.format(ks.getCreateTime()));
							map.put("mac",ipini.getMac());
							map.put("ip",ipini.getIpAddress());
							map.put("interface","eth0");
							installMap.add(map);
						}
					}
				}
				
				ObjectMapper objectMapper = new ObjectMapper();
				Map<String,String> params=new HashMap();
				try {
					params.put("install",objectMapper.writeValueAsString(installMap));
				} catch (JsonProcessingException e) {
					log.error("",e);
					TaskOperator.getInstance().redoTask(task, this);
					return;
				}
				params.put("tid", ""+task.getTid());
				if(!HttpClientUtil.curlPost(isoServer.getServerUrl()+"/install", params))
					TaskOperator.getInstance().redoTask(task, this);
				else{
					task.setNoticeStatus(1);
					task.setTaskStatus(0);
					dao.updateTask(task);
				}
				
//				//x系统安装
//				XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
//				try {
//					config.setServerURL(new URL(isoServer.getServerUrl()));
//					XmlRpcClient client = new XmlRpcClient();
//					client.setConfig(config);
//					
//					//获取安装方案
//					InstallProgram scheme=BeanUtil.getBean(InstallProgramDao.class).getInstallPlanById(task.getSchemeCsaId());
//					
//					//获取安装部署配置
//					RelSchemeKS sks=new RelSchemeKS();
//					sks.setSchCsaId(task.getSchemeCsaId());
//					List<RelSchemeKS> listks=BeanUtil.getBean(ModelDao.class).getRelSchemeKS(sks);
//					KickStart ks=null;
//					if(listks!=null && listks.size()==1){
//						ks=BeanUtil.getBean(KickstartDao.class).findKickStartById(listks.get(0).getKsCsaId());
//					}else{
//						log.error("没有查询到安装方案关联的部署配置！！！");
//						return;
//					}
//					if(ks==null){
//						log.error("安装方案关联的部署配置（CSAID）:"+listks.get(0).getKsCsaId()+"不存在!");
//						return;
//					}						
//				
//					//获取相关IP
//					RelSchemeIP sip=new RelSchemeIP();
//					sip.setSchCsaId(task.getSchemeCsaId());
//					List<RelSchemeIP> listip=BeanUtil.getBean(ModelDao.class).getRelSchemeIP(sip);
//					SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss"); 
//					List<Map<String,String>> installMap=new ArrayList<Map<String,String>>();
//					if(listip!=null){
//						IP ipini=null;
//						Map<String,String> map=null;
//						for (RelSchemeIP ip : listip) {
//							ipini=BeanUtil.getBean(IPDao.class).findById(ip.getIpCsaId());
//							if(ipini!=null){
//								map=new HashMap();
//								map.put("hostName","");
//								map.put("systemName",scheme.getUserAccount()+sdf.format(scheme.getCreateTime()));
//								map.put("profileName",ks.getUserAccount()+sdf.format(ks.getCreateTime()));
//								map.put("mac",ipini.getMac());
//								map.put("ip",ipini.getIpAddress());
//								map.put("interface","eth0");
//								installMap.add(map);
//							}
//						}
//					}
//					Object [] params = new Object[]{installMap.toArray(),task.getTid()};
//					String soInfo = (String) client.execute("exec_install",params);
//					if(soInfo.indexOf("error")<0)
//						BeanUtil.getBean(ModelDao.class).updateTask(task);
//					log.info(soInfo);
//				} catch (Exception e) {
//					log.error("服务器请求失败！",e);
//					TaskOperator.getInstance().redoTask(task, this);
//				}
			}
		}
		
	}
