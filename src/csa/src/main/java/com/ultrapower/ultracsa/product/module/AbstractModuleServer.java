/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.module;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 所有基础服务的抽象类，实现ModuleServer 接口，对于多数基础服务不需要实现stopServer  和 resetServer，因此提供抽象类
 */
public abstract class AbstractModuleServer implements ModuleServer{

	public abstract boolean startServer() throws Exception;
	
	public boolean stopServer() throws Exception{
		return true;
	}
	
	public boolean resetServer() throws Exception{
		return true;
	}
	
	public void addObject(Object obj){
		;
	}
}
