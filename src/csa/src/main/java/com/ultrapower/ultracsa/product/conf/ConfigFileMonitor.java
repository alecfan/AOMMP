/**
 * UltraCMDB平台系统文件
 * 
 * Copyright 2014 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.conf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ultrapower.ultracsa.product.util.dynamic.DynamicUpdateServer;

/**
 * 作者: yangjie
 * 
 * 日期: 2014
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * 用于监视配置文件的修改
 */
public class ConfigFileMonitor extends Thread
{
	private static Logger log = Logger.getLogger(ConfigFileMonitor.class);
    public Map<String,String> fileMap=new HashMap();
    private static ConfigFileMonitor cfm=null;
    
    private ConfigFileMonitor(String name){
        super(name);
    }
    
    public static ConfigFileMonitor getInstance(String name){
        if(cfm==null){
            cfm=new ConfigFileMonitor(name);
        }
        return cfm;
    }

    public void run()
    {
        while(true){
            try
            {
                sleep(10000);
            }
            catch(InterruptedException e)
            {
                log.error("",e);
            }
            doChech();
           
        }
        
    }
    
    /**
     * 注册配置文件加载类
     * @param cfName 配置文件名称
     */
    public synchronized void add(String cfName,String fileName ){
    	if(!fileMap.containsKey(fileName)){
    		fileMap.put(fileName,cfName);
    		doCheck(cfName,fileName);
    	}
    }
    
    private synchronized void doChech(){
    	for (Iterator iterator = fileMap.keySet().iterator(); iterator.hasNext();) {
			String fileName = (String) iterator.next();
			doCheck(fileMap.get(fileName),fileName);
		};
    }
    
    private void doCheck(String cfName,String fileName){
        ConfigFile cf=null;
    	Class cfClass=DynamicUpdateServer.getInstance().getNewClass(cfName);
    	try {
			cf=(ConfigFile)cfClass.newInstance();
		}catch (Exception e) {
			log.error("加载Class："+cfName+" 失败！ ",e);
		}
    	if(cf.isModified(fileName))
            cf.reLoad(fileName);
    }
    
}
