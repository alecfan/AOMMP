/**
 * UltraCSA平台系统文件
 * 
 * Copyright 2016 北京神州泰岳软件股份有限公司
 */
package com.ultrapower.ultracsa.product.pojo;

import java.util.Date;

/**
 * 作者: xtxb
 * 
 * 日期: 2016
 * 
 * 版权说明：北京神州泰岳软件股份有限公司
 * 
 * CSA产品管理的脚本文件的实体类
 */
public class ShellFile {
	/**
	 * 
	 */
	private Integer csaID;
	/**
	 * 文件名称
	 */
	private String fileName;
	/**
	 * 文件大小，单位为KB
	 */
	private Long fileSize;
	/**
	 * 脚本上传用户
	 */
	private String userAccount;
	/**
	 * 脚本第一次上传时间
	 */
	private Date createTime;
	
	private Date updateTime;
	/**
	 * 文件类型，如：巡检、安装
	 */
	private String fileType;
	public Integer getCsaID() {
		return csaID;
	}
	public void setCsaID(Integer csaID) {
		this.csaID = csaID;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Long getFileSize() {
		return fileSize;
	}
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}	
	
}
