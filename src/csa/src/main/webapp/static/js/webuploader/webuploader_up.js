// 文件上传
jQuery(function() {
    var $ = jQuery,
        $list = $('#thelist'),
        $btn = $('#ctlBtn'),
        state = 'pending',
        $alert = null;
        uploader;
    uploader = WebUploader.create({
    	//允许的文件后缀，不带点，多个用逗号分割
    	accept: {
    		extensions: 'iso,zip'
        },
    	//不需要手动调用上传，有文件选择即开始上传
    	auto: true,
        // 不压缩image
        resize: false,
        // swf文件路径，需要用到flash的时候BASE_URL自己根据需要定义 也可写成绝对路径
        swf: 'Uploader.swf',
        // 文件接收服务端。此处根据自己的框架写，本人用的是SpringMVC
        server: 'up',   
        // 选择文件的按钮。可选。
        // 内部根据当前运行是创建，可能是input元素，也可能是flash.
        pick: '#picker',
        //是否要分片处理大文件上传
        chunked: false,
        //分片大小，默认5M
        chunkSize: 10 * 1024 * 1024,
        //如果某个分片由于网络问题出错，允许自动重传多少次，默认2次
        chunkRetry: 2,
        //允许同时最大上传进程数
        threads: 1,
        //验证文件总数量, 超出则不允许加入队列
        fileNumLimit: 5
        //验证文件总大小是否超出限制, 超出则不允许加入队列
        //fileSizeLimit,
        //验证单个文件大小是否超出限制, 超出则不允许加入队列
        //fileSingleSizeLimit
    });

    // 当有文件添加进来的时候
    uploader.on( 'fileQueued', function( file ) {
        $list.append( '<div id="' + file.id + '" class="item">' +
            /*'<h4 class="info">' + file.name + '</h4>' +
            '<div class="alert alert-info" role="alert">' + file.name + '</div>' +*/
            '<div id="111" class="alert alert-info  alert-dismissible" role="alert"><strong>' + file.name + '</strong> 等待上传...</div>' +
        '</div>' );
    });

    // 文件上传过程中创建进度条实时显示。
    uploader.on( 'uploadProgress', function( file, percentage ) {
        var $li = $( '#'+file.id ),
            $percent = $li.find('.progress .progress-bar');

        // 避免重复创建
        if ( !$percent.length ) {
            $percent = $('<div class="progress">' +
              '<div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" style="width: 0%">' +
              '</div>' +
            '</div>').appendTo( $li ).find('.progress-bar');
            $li.find('div.alert.alert-info').html('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' + file.name + '</strong> 上传中...');
        }
        $percent.css( 'width', percentage * 100 + '%' );
        $percent.text(parseInt(percentage * 100) + '%');
    });

    uploader.on( 'uploadSuccess', function( file ) {
        $.ajax({
	        type:'POST',
	        dataType:'json',
	        url:'insertFile',
	        data:{'fileName':file.name},
	        success:function(data){
	            $( '#'+file.id ).find('div.alert.alert-info').html('<strong>' + file.name + '</strong> 已上传！');
	            var refresh = true;
	            $('div.alert.alert-info').each(function(){
	            	if($(this).text().indexOf('上传中')!=-1||$(this).text().indexOf('等待上传')!=-1){
	            		refresh = false;
	            	}
	            });
	            if(refresh){
	            	location.reload();
	            }
	        }
    	});
    });

    uploader.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('div.alert.alert-info').html('<strong>' + file.name + '</strong> 上传出错！');
    });

    uploader.on( 'uploadComplete', function( file ) {
        $( '#'+file.id ).find('.progress').fadeOut();
    });
    
    uploader.on( 'beforeFileQueued', function( file ) {
    	var fileName = file.name;
    	if(!fileName.endsWith("-x86_64.iso")&&!fileName.endsWith("-x86_32.iso")){
    		uploader.removeFile(file,true);
    		swal('', '上传镜像名称必须以-x86_64或-x86_32结尾!', 'error')
    		return false;
    	}
    });
    uploader.on( 'all', function( type ) {
        if ( type === 'startUpload' ) {
            state = 'uploading';
        } else if ( type === 'stopUpload' ) {
            state = 'paused';
        } else if ( type === 'uploadFinished' ) {
            state = 'done';
        }

    });

    $list.on('click','.close',function () {
    	var fileId = $(this).parent().parent().attr('id');
  	    uploader.cancelFile(fileId);
  	    uploader.removeFile(fileId,true);
  	    $( '#'+fileId ).find('.progress').fadeOut();
    });
});