function refreshTab(){
	$.ajax({
	     type: 'POST',
	     url: "getTabList" ,
	    success: function(data){
	    	//获取当先选中的标签页
	    	var currentType=$("#tabList a[class='current']").val();
	    	$("#tabList a").remove();  
	    	if(data.length>0){
	    		if(currentType==undefined || data.indexOf(currentType)<0)
	    			currentType=data[0];
	    		var newRow="";
		    	for(index in data){
		    		if(data[index]==currentType){
		    			newRow="<a class=\"current\"  href=\"javascript:void(0);\" >"+data[index]+"</a>";
		    		}else{
		    			newRow="<a href=\"javascript:void(0);\" >"+data[index]+"</a>";
		    		}
		 	      $("#tabList").append(newRow);
		    	}
	    	}
	    	refreshTable(currentType);
	    } ,
	    error: function(data){
	    	

	    },
	    dataType: "json"
	});
}

//快速查询回调函数
function quicSsearchCallback(){
	var fileName=$('#fileName_search').val();  
	if(fileName=="" || fileName==msg.inputshellname){
		swal(msg.inputshellname, "", "warning");
		return;
    }
	$.ajax({
	     type: 'POST',
	     url: "findFile" ,
	     data: "fileName="+fileName ,
	     success: function(data){
	    	 setTableValue(data);
	     } ,
	     error: function(msgdata){
	 		swal(msg.FindFileError, "", "warning");
	     },
	     dataType: "json"
	});
}

/**
 * 刷新表格，标签页触发
 */
function refreshTable( filetype){
	$.ajax({
	     type: 'POST',
	     url: "shellListbyType" ,
	    data: "fileType="+filetype ,
	    success: function(data){
	    	setTableValue(data);
	    } ,
	    error: function(data){
	    	
	    },
	    dataType: "json"
	});
}

/**
 * 根据查询条件刷新表格
 */
function setTableValue(data){
	$(".csa_tabletop").css("visibility","hidden");
	$('#shellTable tr').remove();  
	var newRow='<tr>'+
                        '<th scope=\"col\"><input id=\"1-checkbox\"  type=\"checkbox\" value=\"\" />'+msg.ShellType+'</th>'+
                        '<th scope=\"col\">'+msg.FileSize+'</th>'+
                        '<th scope=\"col\">'+msg.uploadUser+'</th>'+
                        '<th scope=\"col\">'+msg.uploadTime+'</th>'+
                        '</tr>';
	$('#shellTable').append(newRow);
	for (row in data){
	       newRow = "<tr>"+
                          "<td><input id=\""+data[row].csaID+"\"  name=\"flag\"  type=\"checkbox\" value=\"\" />"+data[row].fileName+"</td>"+
                          "<td>"+data[row].fileSize+"</td>"+
                          "<td>"+data[row].userAccount+"</td>"+
                          "<td>"+data[row].createTime+"</td>"+
                          "</tr>";
	      $('#shellTable').append(newRow);
	}
}

/**
 * 自定义按钮触发文件选择窗口
 */
function selectFile(){
	$("#fileid").click();
}

/**
 * 将选择的文件名显示到文本矿中
 */
function setFileDisplayName(){
	var fn=$("#fileid").val();
	if(fn.indexOf(".sh")<=0 && fn.indexOf(".txt")<=0 && fn.indexOf(".yml")<=0){
		swal(msg.SelectFileupload, "", "warning");
		return;
	}
	var fileName=$("#fileid").val();
	if(fileName.indexOf("\\")>0){
		fileName=fileName.substring(fileName.lastIndexOf("\\")+1);
	}
	$("#fileName").val(fileName);
}

/**
 * 提交文件上传动作
 */
function filesubmit(){
	var fn=$("#fileName").val();
	if(fn==null || fn==""){
		swal(msg.SelectFileupload, "", "warning");
		return;
	}
	checkFileName();
}

/**
 * 检查脚本文件是够已经存在
 */
function checkFileName(){
	var fileName=$("#fileName").val();
	if(fileName.indexOf("\\")>0){
		fileName=fileName.substring(fileName.lastIndexOf("\\")+1);
	}
	$.ajax({
	     type: 'POST',
	     url: "checkFileName" ,
	     data: "fileName="+fileName ,
	     success: function(msgdata){
	    	 if(msgdata==false){
	    		 var ajax_option={
	    				url:"uploadShell",//默认是form action
	    				success:function(data){
	    					if(data=="-1")
	    					   swal(msg.uploadFileError, "", "error");
	    					else{
	    						createShellFile(data);
	    					}
	    				},
	    		        error:function(data){
	    		        	 swal(msg.uploadFileError, "", "error");
	    		        }
	    		};
	    		$('#shellfileupload').ajaxSubmit(ajax_option);
	     	}else
	     		swal(msg.FileHasUpload);
	    	 $("#fileid").val("");
	     } ,
	     error: function(msgdata){
	    	 swal(msg.CheckFileupload, "", "error");
	     },
	     dataType: "json"
	});
}

/**
 * 数据库中保存脚本文件
 */
function  createShellFile(size){
	var fileName=$("#fileName").val();
	$.ajax({
	     type: 'POST',
	     url: "createShellFile" ,
	     data: "fileNama="+$("#fileName").val()+"&fileSize="+size+"&fileType="+$("#filletype").val() ,
	     success: function(data){
	    	 refreshTab();
	    	$('#myModal').modal('hide')
	     } ,
	     error: function(data){
	    	 swal(msg.uploadFileError, "", "error");
	     },
	     dataType: "json"
	});
}

/**
 * 删除脚本文件
 */
function deleteFile(){
	var checkedList=$("input:checkbox:checked[name='flag']");
	var fileID="";
	$.each(checkedList,function(ss,fff){
		fileID+=(fff.id+",");
	});
	fileID=fileID.substring(0,fileID.length-1);
    swal({
        title: msg.ConfirmDelete,
        text: "",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        confirmButtonText: msg.ConfirmDelete_delete,
        confirmButtonColor: "#2a94e4",
        cancelButtonText: msg.ConfirmDelete_cancel,
    }, function() {
    	$.ajax({
		     type: 'POST',
		     url: "deleteShellFile" ,
		     data: "fileids="+fileID,
		     success: function(data){
		    	 refreshTab();
		     } ,
		     error: function(data){
		    	 swal(msg.DeleteError, "", "error");
		     },
		     dataType: "json"
		});
    });
}
