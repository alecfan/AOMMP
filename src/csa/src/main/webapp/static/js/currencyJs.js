/**
 * the action for checkbox
 * 
 * @param data
 *            dom for the selected checkbox
 * 
 */
var selectCheckBox = function(data) {
	var id = $(data).prop("id");
	var checked = $(data).is(':checked');
	if (id == "allCheck") {
		// select of unselect all checkbox
		var trs = $(data).parent().parent().siblings();
		trs.each(function() {
			$(this).find("td input[type='checkbox']").prop("checked", checked);
		});
	}

//	table = $(data).parent().parent().parent();
	var trs = $(data).parent().parent().parent().find('tr');
	var tempChecked = false;
	var count = 0;
	trs.each(function() {
		if($(this).index() == 0)
			return true;
		tempChecked = $(this).find("input[type='checkbox']").is(
					':checked');
		//计数被选择的个数
		if (tempChecked)
			count++;
	});

	if (count > 0) {
		$('#optionDiv').css("visibility", "visible");
		// 检查某些按钮是否允许操作
		if(count == trs.length-1){
			if (count == 1){
				$('#allCheck').prop("checked", "checked");
				$('#update').prop('class', 'bodrad5 edit');
				$('#update').attr("onclick", "addOrUpdate('update')");
				$('#copyKs').prop('class', 'bodrad5 edit');
				$('#copyKs').attr("onclick", "copyKickstart()");
			}else{
				$('#allCheck').prop("checked", "checked");
				$('#update').prop('class', 'bodrad5');
				$('#update').removeAttr('onclick');
				$('#copyKs').prop('class', 'bodrad5');
				$('#copyKs').removeAttr('onclick');
			}
		}else if (count == 1){
			$('#allCheck').prop("checked", false);
			$('#update').prop('class', 'bodrad5 edit');
			$('#update').attr("onclick", "addOrUpdate('update')");
			$('#copyKs').prop('class', 'bodrad5 edit');
			$('#copyKs').attr("onclick", "copyKickstart()");
		}else {
			$('#allCheck').prop("checked", false);
			$('#update').prop('class', 'bodrad5');
			$('#update').removeAttr('onclick');
			$('#copyKs').prop('class', 'bodrad5');
			$('#copyKs').removeAttr('onclick');
		}
		fillI18n(count);
	} else {
		$('#allCheck').prop("checked", false);
		$('#optionDiv').css("visibility", "hidden");
	}
	//在这里添加显示勾选了几行数据的逻辑
	
	

};

/** 
 * 函数名：   validateIP 
 * 函数功能： 验证IP的合法性 
 * 传入参数： what:点分十进制的IP(如：192.168.1.2)  
 * 返回值：   true:what为合法IP false: what为非法IP 
 **/
var validateIP = function(what) {
	return /^(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)$/i.test( what );
	
//	if (what.search(/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/) == -1)
//		return false;
//	var fs = 0, ls = 0;
//	var myArray = what.split(/\./);
//	var i;
//	for (i = 0; i < 4; i++) {
//		if (isNaN(myArray[i]))
//			return false;
//
//		var t = parseInt(myArray[i]); /* 每个域值范围0-255 */
//		if ((t < 0) || (t > 255))
//			return false;
//	}
//	fs = parseInt(myArray[0]); //取第一位进行校验  
//	ls = parseInt(myArray[3]); //取最后一位进行校验  
//
//	/* 主机部分不能全是1和0（第一位不能为255和0），网络部分不能全是0（最后一位不能为0） */
//	if ((fs == 255) || (fs == 0) || (ls == 0)) {
//		return false;
//	}
//	return true;
};
/** 
 * 函数名：   validateMask 
 * 函数功能： 验证子网掩码的合法性 
 * 传入参数： MaskStr:点分十进制的子网掩码(如：255.255.255.192)  
 * 调用函数： _checkIput_fomartIP(ip)  
 * 返回值：   true:MaskStr为合法子网掩码 false: MaskStr为非法子网掩码 
 **/
var validateMask = function(MaskStr) {
	var exp = /^(254|252|248|240|224|192|128|0)\.0\.0\.0|255\.(254|252|248|240|224|192|128|0)\.0\.0|255\.255\.(254|252|248|240|224|192|128|0)\.0|255\.255\.255\.(254|252|248|240|224|192|128|0)$/;
	var reg = MaskStr.match(exp);
	if (reg == null) {
		return false; // "非法"
	} else {
		return true; // "合法"
	}
};
/**
 * 函数名： validateMac 函数功能： 验证mac的合法性 传入参数： mac:点分十进制的mac 返回值： true:mac为合法mac
 * false: mac为非法mac
 */
var validateMac = function(mac) {
	var temp = /^[A-F0-9]{2}(:[A-F0-9]{2}){5}$/;
	if (!temp.test(mac)) {
		return false;
	}
	return true;
};
/**
 * 生成随机密码
 * @param length 密码长度
 * @param special  是否包含特殊字符
 * @returns {String}
 */
function password(length, special) {
	  var iteration = 0;
	  var password = "";
	  var randomNumber;
	  while(iteration < length){
	    randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
	    if(!special){
	      if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
	      if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
	      if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
	      if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
	    }
	    iteration++;
	    password += String.fromCharCode(randomNumber);
	  }
	  return password;
	}
