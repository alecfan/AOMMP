<%@ page language="java" pageEncoding="UTF-8" isErrorPage="true"%>
<%
	String inventory_errors_err=String.valueOf(request.getAttribute("javax.servlet.error.status_code"));
	String inventory_errors_message=String.valueOf(request.getAttribute("javax.servlet.error.message"));
	String inventory_exception_type=String.valueOf(request.getAttribute("javax.servlet.error.exception_type"));
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="csa/err/images/error.css" rel="stylesheet" type="text/css" />
		<title><%=inventory_errors_err %>错误</title>
	</head>
	
	<body>
		<div class="container">
			<div class="errorContent3">
				<h1 class="red">
					<%
					out.println("请与管理员联系!");
					%>
				</h1>
				<hr>
			</div>
		</div>
	</body>
</html>
