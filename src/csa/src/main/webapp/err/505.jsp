<%@ page language="java" pageEncoding="UTF-8" isErrorPage="true"%>
<% Exception ex = (Exception) request.getAttribute("Exception");%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="/csa/err/images/error.css" rel="stylesheet" type="text/css" />
		<title>错误</title>
	</head>
	
	<body>
		<div class="container">
			<div class="errorContent4">
				<h1 class="red">
					<%
					out.println("请与管理员联系!");
					%>
				</h1>
				<h2  class="red"><%=ex.getMessage() %></h2>
				<hr>
			</div>
		</div>
	</body>
</html>
