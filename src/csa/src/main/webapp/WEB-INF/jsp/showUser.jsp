<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>  
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">  
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>  
  <head>  
    <title>测试</title>  
  </head>  
    
  <body>
  	 <!--  循环后台通过model传递的list -->
    <%-- <c:forEach items="${userList}" var="user">
	     用户名: ${user.userName}
	</c:forEach> --%>
	<!-- 后台接受Article对象 -->
	<%-- <form action="${pageContext.request.contextPath}/showUser" method="post">
		<input name="title" value="" /><br>
		<input name="content" value="" /><br>
		<input name="user.userName" value="" /><br>
		<input name="user.password" value="" /><br>
		<input name="user.age" value="" /><br>
		<button type="submit"></button>
	</form> --%>
	
	
	<!-- 这里的UserListForm对象里面的属性被定义成List，而不是普通自定义对象。
	所以，在表单中需要指定List的下标。
	值得一提的是，Spring会创建一个以最大下标值为size的List对象，
	所以，如果表单中有动态添加行、删除行的情况，就需要特别注意，
	譬如一个表格，用户在使用过程中经过多次删除行、增加行的操作之后，下标值就会与实际大小不一致，
	这时候，List中的对象，只有在表单中对应有下标的那些才会有值，否则会为null -->
	<%-- <form action="${pageContext.request.contextPath}//testListForm" method="post">
	<table>
	<thead>
	<tr>
	<th>userName</th>
	<th>password</th>
	</tr>
	</thead>
	<tfoot>
	<tr>
	<td colspan="2"><input type="submit" value="Save" /></td>
	</tr>
	</tfoot>
	<tbody>
	<tr>
	<td><input name="users[0].userName" value="" /></td>
	<td><input name="users[0].password" value="" /></td>
	</tr>
	<tr>
	<td><input name="users[1].userName" value="" /></td>
	<td><input name="users[1].password" value="" /></td>
	</tr>
	<tr>
	<td><input name="users[2].userName" value="" /></td>
	<td><input name="users[2].password" value="" /></td>
	</tr>
	</tbody>
	</table>
	</form> --%>
	
  </body>  
</html>  
