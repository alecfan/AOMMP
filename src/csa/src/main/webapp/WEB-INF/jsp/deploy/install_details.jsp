<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CSA</title>
<link href="static/css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="static/css/sweetalert.css" rel="stylesheet" type="text/css"/>
<link href="static/css/base.css" rel="stylesheet" type="text/css" />
<link href="static/css/main.css" rel="stylesheet" type="text/css" />
<link href="static/css/webuploader/webuploader.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<jsp:include page="../include/nav.jsp">
		<jsp:param value="deploy" name="menu" />
		<jsp:param value="installDetails" name="menusub" />
	</jsp:include>
<div class="modal fade bs-example-modal-lg" id="logModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<!--content-->
<div class="content">
	<!--tab&btn&search-->
	<div class="btn_search">
    	<div class="csa_tab bodrad5 bg_white"><a class="current" href="#NotInstalled" data-toggle="tab">未安装</a><a href="#processing" data-toggle="tab">进行中</a><a href="#completed" data-toggle="tab">已完成</a><a href="#failed" data-toggle="tab">安装失败</a></div>
        <div class="csa_search bodrad5 bg_white"><input id="searchInput" type="text" value="" placeholder="输入您要找的IP" /><a href="javascript:void(0);"></a></div>
    </div>
    <!---->
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane fade in active" id="NotInstalled" ipstatus="01">
	  	<div class="csa_list bodrad5 bg_white">
        <div class="csa_table">
        </div>
    	</div>
	  </div>
	  <div role="tabpanel" class="tab-pane fade" id="processing" ipstatus="02">
	  	<div class="csa_list bodrad5 bg_white">
        <div class="csa_table">
        </div>
    	</div>
	  </div>
	  <div role="tabpanel" class="tab-pane fade" id="failed" ipstatus="03">
	  	<div class="csa_list bodrad5 bg_white">
        <div class="csa_table">
        </div>
    	</div>
	  </div>
	  <div role="tabpanel" class="tab-pane fade" id="completed" ipstatus="04">
	  	<div class="csa_list bodrad5 bg_white">
        <div class="csa_table">
        </div>
    	</div>
	  </div>
	</div>
	    <div id="callBackPager" style="text-align:center;"></div>
</div>
<!--footer-->
<div class="cas_foot"><span class="mr30">版权所有 © 2016</span>神州泰岳 UltraPower</div>
<script type="text/javascript" src="static/js/jquery-2.2.1.min.js"></script>
<script src="static/js/sweetalert.min.js"></script>
<script type="text/javascript" src="static/css/bootstrap/js/bootstrap.js" ></script>
<script type="text/javascript" src="static/js/extendPagination.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.tab-content').on('click','a', function () {
			$.ajax({  
		        type:'POST',
		        dataType:'html',
		        url:'getInstallLog',
		        data:{
	                "logName":$(this).attr("ipAddress"),
	            },
		        success:function(data){
		        	if(data=='FileNotFound'){
		        		swal("找不到日志!", "", "warning");
		        	}else{
		        		$('#logModal').modal({
		    			});
		        		$('#logModal .modal-content').html(data);
		        	}
		        }
		    });
		});
		//标签页
		$('.csa_tab a').click(function (e) {
			$('a.current').removeClass('current');
			$(this).addClass('current');
		});
		callBackPagination('#NotInstalled');
		$('.csa_tab.bodrad5.bg_white a').on('shown.bs.tab', function () {
			callBackPagination($(this).attr('href'));
		});
		$('.csa_search.bodrad5.bg_white a').click(function () {
			callBackPagination($('.csa_tab.bodrad5.bg_white a.current').attr('href'));
		});
		$('.csa_search.bodrad5.bg_white').on("keydown","input[type='text']", function (e) {
	        var key = e.which;
	        if (key == 13) {
	        	callBackPagination($('.csa_tab.bodrad5.bg_white a.current').attr('href'));
	        }
	    });
		$('.csa_search.bodrad5.bg_white').on("mouseover mouseout","a",function(event){
			 if(event.type == "mouseover"){
				 $(this).css("background-color","#2868c8");
			 }else if(event.type == "mouseout"){
				 $(this).css("background-color","#2a94e4");
			 }
		})
	})
    function callBackPagination(type) {
		//每页显示数据量（默认20）
        var limit = 20;
		//分页栏展示数（默认10）
        var showCount = 10;
        var data = getInstallDetails(limit,1);
        var totalCount = data.totalCount;
        createTable(1, limit, totalCount,type);
        $('#callBackPager').extendPagination({
            totalCount: totalCount,
            showCount: showCount,
            limit: limit,
            callback: function (curr, limit, totalCount,type) {
                createTable(curr, limit, totalCount,type);
            }
        });
    }
    function createTable(currPage, limit, total,type) {
    	var data = getInstallDetails(limit,currPage);
        var IPList = data.IPList;
        if(IPList.length<1&&currPage>1){
        	callBackPagination('#'+$('.tab-pane.fade.in.active div').attr('id'));
        }else if(IPList.length<1&&currPage<=1){
        	$('.tab-pane.fade.in.active div.csa_table').html('<div class="alert alert-info" role="alert" style="margin-bottom: 0px;"><strong>提示!</strong>没有相关数据</div>');
        }else{
        	var html = [], showNum = limit;
            if (total - (currPage * limit) < 0) showNum = total - ((currPage - 1) * limit);
            html.push(' <table width="100%" border="0" cellspacing="0" cellpadding="0">');
            html.push(' <thead><tr><th width="100" scope="col">装机IP</th><th scope="col">所属ISO镜像库</th><th scope="col">状态</th><th scope="col">开始时间</th><th scope="col">备注</th></tr></thead><tbody>');
            $.each(IPList, function(i) {
                html.push('<tr><td>' + this.ipAddress + '</td>');
                html.push('<td>' + this.server.ip + '</td>');
                if(this.ipStatus=='01'){
                	html.push('<td>未安装</td>');
                }else if(this.ipStatus=='02'){
                	html.push('<td>安装中</td>');
                }else if(this.ipStatus=='03'){
                	html.push('<td>安装失败</td>');
                }else if(this.ipStatus=='04'){
                	html.push('<td>已安装</td>');
                }
                html.push('<td>'+ format(this.createTime, 'yyyy-MM-dd HH:mm:ss') +'</td>');
                html.push('<td><a href="###" ipAddress="'+ this.ipAddress +'">安装日志</a></td>');
                html.push('</tr>');
            });
            html.push('</tbody></table>');
            var mainObj = $('.tab-pane.fade.in.active div.csa_table');
            mainObj.empty();
            mainObj.html(html.join(''));
        }
    }
    function getInstallDetails(limit,currPage){
    	var dataJson = null;
    	$.ajax({  
	        type:'GET',
	        async:false,
	        dataType:'json',
	        url:'getInstallDetails',
	        data:{
                "pageSize":limit,
                "pageNo":currPage,
                "ipstatus":$('.tab-pane.fade.in.active').attr('ipstatus'),
                "search":$('#searchInput').val(),
            },
	        success:function(data){
	        	dataJson = data;
	        }
	        });
    	return dataJson;
    }
    var format = function(time, format){
        var t = new Date(time);
        var tf = function(i){return (i < 10 ? '0' : '') + i};
        return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function(a){
            switch(a){
                case 'yyyy':
                    return tf(t.getFullYear());
                    break;
                case 'MM':
                    return tf(t.getMonth() + 1);
                    break;
                case 'mm':
                    return tf(t.getMinutes());
                    break;
                case 'dd':
                    return tf(t.getDate());
                    break;
                case 'HH':
                    return tf(t.getHours());
                    break;
                case 'ss':
                    return tf(t.getSeconds());
                    break;
            }
        })
    }
</script>	
</body>
</html>