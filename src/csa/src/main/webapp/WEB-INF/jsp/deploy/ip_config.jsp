<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CSA</title>
<link href="static/css/bootstrap/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<!-- <link href="static/css/bootstrap/css/bootstrap.min.css" type="text/css" /> -->
<link href="static/css/base.css" rel="stylesheet" type="text/css" />
<link href="static/css/main.css" rel="stylesheet" type="text/css" />
<link href="static/css/webuploader/webuploader.css" rel="stylesheet"
	type="text/css" />
<link href="static/css/sweetalert.css" rel="stylesheet" type="text/css"/>

<script src="static/js/sweetalert.min.js"></script>
<script type="text/javascript" src="static/js/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="static/js/jquery.form.min.js"></script>
<!-- 公共的js -->
<script type="text/javascript" src="static/js/currencyJs.js"></script>
<!-- 上传组件 -->
<script type="text/javascript"
	src="static/js/webuploader/webuploader.js"></script>
	
<!-- 搜索组件 -->
<script type="text/javascript" src="static/js/search.js"></script>

<style>
.pages {
	text-align: center;
	margin: 30px 0;
}

.pages li a {
	font-size: 14px
}
</style>
</head>
<body>
	<div style="display: none">
		<input type="hidden" id="bathPath"
			value="${pageContext.request.contextPath}" /> <input type="hidden"
			id="user" value="${userName }" /> <input type="hidden"
			id="optionType" /> <input type="hidden" id="trIndex" /> <input
			type="hidden" id="updateId" /> <input type="hidden"
			id="batchInsertMsg"
			value='<spring:message code="bathcInsertMsg"></spring:message>' /> <input
			type="hidden" id="curPage" value="${curPage }" /> <input
			type="hidden" id="showPage" value="${showPage }" /> <input
			type="hidden" id="showCount" value="${showCount }" /> <input
			type="hidden" id="totalCount" value="${totalCount }" /> <input
			type="hidden" id="next"
			value='<spring:message code="next"></spring:message>' /> <input
			type="hidden" id="previous"
			value='<spring:message code="Previous"></spring:message>' /> <input
			type="hidden" id="ipHasInsMsg"
			value='<spring:message code="ipHasInsMsg"></spring:message>' /> <input
			type="hidden" id="fileName" />

	</div>

	<form id="ipfileupload" class="form-horizontal" method="post"
		enctype="multipart/form-data" accept-charset="UTF-8">
		<input id="fileid" name="file" type="file" style="display: none"
			onchange="setFile()" accept=".xlsx;*.xls;*.et" />
	</form>

	<jsp:include page="../include/nav.jsp">
		<jsp:param value="deploy" name="menu" />
		<jsp:param value="ipConfig" name="menusub" />
	</jsp:include>

	<div class="content">
		<div class="btn_search">
			<div class="csa_btn">
				<a href="javascript:void(0)" onclick="addOrUpdate('add')"><spring:message
						code="ipAllocation"></spring:message></a>
			</div>
			<%-- 
			<div class="csa_btn">
				<a href="javascript:void(0)" id=""importIp""><spring:message
						code="import_from_excel"></spring:message></a>
			</div> --%>
			<div class="csa_btn">
				<a href="javascript:void(0)" id="upload" onclick="uploadFile()"><spring:message
						code="import_from_excel"></spring:message></a>
			</div>
			<div class="csa_btn">
				<a href="downLoadTemplateFile?method=downLoadFile"><spring:message
						code="downImportTemplate"></spring:message></a>
			</div>
			<div class="csa_search bodrad5 bg_white">
				<input type="text" id="ip_search" value="" placeholder='<spring:message code="IpsearchTip"></spring:message>'/><a
					href="javascript:void(0)"></a>
			</div>
		</div>
		<div class="csa_list bodrad5 bg_white">
			<div class="csa_table">
				<div id="optionDiv" class="csa_tabletop" style="visibility:hidden;">
					<span id="selectedlen">已选 3 个文件</span><a id="deleteIps" class="bodrad5 edit"
						href="javascript:void(0)" onclick="deleteIps()"><spring:message
							code="delete"></spring:message></a><a id="update"
						class="bodrad5 edit" href="javascript:void(0)"
						onclick="addOrUpdate('update')" style="disabled:disabled"><spring:message
							code="update"></spring:message></a>
				</div>
				<table id="ip_table" width="100%" border="0" cellspacing="0"
					cellpadding="0">
					<tr>
						<th scope="col"><input type="checkbox" id="allCheck"
							value="allCheck" onclick="selectCheckBox(this)" /> <spring:message
								code="adStIp"></spring:message></th>
						<th scope="col"><spring:message code="Mask"></spring:message></th>
						<th scope="col"><spring:message code="MAC"></spring:message></th>
						<th scope="col"><spring:message code="Batch"></spring:message></th>
						<th scope="col"><spring:message code="Creator"></spring:message></th>
						<th scope="col"><spring:message code="CreationTime"></spring:message></th>
						<th scope="col"><spring:message code="Installed"></spring:message></th>
						<th scope="col"><spring:message code="Remark"></spring:message></th>
					</tr>
				</table>
			</div>
		</div>
		<div class="pages">
			<div id="callBackPager"></div>
		</div>
	</div>
	<!--footer-->
	<div class="cas_foot"><span class="mr30">版权所有 © 2016</span>神州泰岳 UltraPower</div>

	<script type="text/javascript"
		src="static/css/bootstrap/js/bootstrap.min.js"></script>
	<!-- 分页 -->
	<script type="text/javascript" src="static/js/page.js"></script>
	<!-- ip 配置js -->
	<script type="text/javascript" src="static/js/ipMain.js"></script>
	<!--popdiv os3_ipfp -->
	<div id="os3_ipfp" style="display:none">
		<div class="popdiv-mask">
			<div class="popdiv">
				<h2><spring:message code="ipAllocation"></spring:message>
				<a class="ico_close" href="javascript:void(0)"
					onclick="closePop(this)"></a></h2>
				<div class="popdiv_cnt">
					<ul class="ipfp">
						<li><b> <spring:message code="Batch"></spring:message></b>
							<div class="ipfpcn">
								<input class="inptext" type="text" id="bantch_inp" value="" />
							</div>
						</li>
						<li><b> <spring:message code="adStIp"></spring:message></b>
							<div class="ipfpcn">
								<%-- <input class="inptext" type="text" id="ip_inp" placeholder="<spring:message code="ipFormat"></spring:message>" value="" /> --%>
								<span><input id="ip_1" class="inptext"  type="text" maxlength='3' value=""></span><em>.</em><span><input class="inptext" id="ip_2" type="text" maxlength='3' value=""></span><em>.</em><span><input class="inptext" id="ip_3" type="text" maxlength='3' value=""></span><em>.</em><span><input class="inptext" id="ip_4" maxlength='3' type="text" value=""></span>
							</div>
						</li>
						<li><b> <spring:message code="Mask"></spring:message></b>
							<div class="ipfpcn">
								<%-- <input class="inptext" id="mask_inp" type="text" placeholder="<spring:message code="maskFormat"></spring:message>" id="bantch_inp" value="" /> --%>
								<span><input class="inptext" id="mask_1" type="text" maxlength='3' value=""></span><em>.</em><span><input class="inptext" id="mask_2" type="text" maxlength='3' value=""></span><em>.</em><span><input class="inptext" id="mask_3" type="text" maxlength='3' value=""></span><em>.</em><span><input class="inptext" id="mask_4" type="text" maxlength='3' value=""></span>
							</div>
						</li>
						<li><b> <spring:message code="MAC"></spring:message></b>
							<div class="ipfpcn">
								<input class="inptext" id="mac_inp" type="text" placeholder="<spring:message code="macFormat"></spring:message>" id="bantch_inp" value="" />
							</div>
						</li>
						<li id="note_div"><b> <spring:message code="UpdateNote"></spring:message></b>
							<div class="ipfpcn" >
								<textarea id="note_inp" placeholder="<spring:message code="noteFormat"></spring:message>"></textarea>
							</div>
						</li>
						<!-- <li class="pop_error">错误提示</li> -->
					</ul>
				</div>
				<div class="popdiv_btn">
					<a class="btnblue bodrad5" href="javascript:void(0)"
						onclick="check_save()"><spring:message code="submit"></spring:message></a>
				</div>
			</div>
		</div>
	
	</div>

	<script type="text/javascript">
	
	

	var createTable = function(data){
		var table = $('#ip_table');
		table.empty();
		var thead = '<tr><th scope="col"><input type="checkbox" id="allCheck" value="allCheck" onclick="selectCheckBox(this)" /> <spring:message code="adStIp"></spring:message></th><th scope="col"><spring:message code="Mask"></spring:message></th><th scope="col"><spring:message code="MAC"></spring:message></th><th scope="col"><spring:message code="Batch"></spring:message></th><th scope="col"><spring:message code="Creator"></spring:message></th><th scope="col"><spring:message code="CreationTime"></spring:message></th><th scope="col"><spring:message code="Installed"></spring:message></th><th scope="col"><spring:message code="Remark"></spring:message></th></tr>';
		table.append(thead);
		$.each(data, function(name, value) {
			var tr = $('<tr>');
			tr.append('<td><input type="checkbox" id="'+this.csaId+'" value="'+this.csaId+'" onclick="selectCheckBox(this)" /><span id="ipSapn">'+this.ipAddress+'</span></td><td>'+this.mask+'</td><td>'+this.mac+'</td><td>'+this.batChid+'</td><td>'+this.userAccount+'</td>');
			var tempDate = this.createTime;
			tr.append('<td>'+tempDate+'</td>');
			//pattern="yyyy/MM/dd" />
			var status = this.ipStatus;
			if(status == '1' || status == 1){
				tr.append('<td><spring:message code="NoInstll"></spring:message></td>');
			}else if(status == '2' || status == 2){
				tr.append('<td><spring:message code="Instlling"></spring:message></td>');
			}else if(status == '3' || status == 3){
				tr.append('<td><spring:message code="InstllError"></spring:message></td>');
			}else {
				tr.append('<td><spring:message code="InstllSuccess"></spring:message></td>');
			}
			tr.append('<td><a class="bodrad5 edit" href="javascript:void(0)" onclick="viewDetail(this)"><spring:message code="updateHistory"></spring:message></a></td>');
			table.append(tr);
		});
	};
	
	
	
		/**
		 * 点击添加按钮事件
		 * 
		 */
		function check_save() {
			var ul = $('#os3_ipfp .popdiv-mask .popdiv .popdiv_cnt ul');
			ul.find('.pop_error').remove();
			var opType = $('#optionType').val();
			//clear all error message
			clearAllError();
			var checkType = true;

			var batch = $('#bantch_inp').val().replace(/(^\s*)|(\s*$)/g, "");
			if (batch == "") {
				ul.append('<li class="pop_error"><spring:message code="batchErrorMsg1"></spring:message></li>');
				return false;
			}
			var ip = $("#ip_1").val().replace(/(^\s*)|(\s*$)/g, "")+'.'+ $("#ip_2").val().replace(/(^\s*)|(\s*$)/g, "")+ '.'+ $("#ip_3").val().replace(/(^\s*)|(\s*$)/g, "")+ '.'+ $("#ip_4").val().replace(/(^\s*)|(\s*$)/g, "");
			var ipFormat = validateIP(ip, "");
			if (!ipFormat) {
				ul.append('<li class="pop_error"><spring:message code="ipErrorMsg1"></spring:message></li>');
				return false;
			} else {
				ipFormat = checkNameIsExist("ip_table", ip, 1, opType);
				if (ipFormat) {
					//已经存在了
					ul.append('<li class="pop_error"><spring:message code="ipErrorMsg2"></spring:message></li>');
					return false;
				}
			}

			var mask = $("#mask_1").val().replace(/(^\s*)|(\s*$)/g, "")+ '.'+ $("#mask_2").val().replace(/(^\s*)|(\s*$)/g, "")+ '.'+ $("#mask_3").val().replace(/(^\s*)|(\s*$)/g, "")+ '.'+ $("#mask_4").val().replace(/(^\s*)|(\s*$)/g, "");
			var maskFormat = validateMask(mask);
			if (!maskFormat) {
				ul.append('<li class="pop_error"><spring:message code="maskErrorMsg1"></spring:message></li>');
				return false;
			}

			var mac = $("#mac_inp").val();
			var macFormat = validateMac(mac);
			if (!macFormat) {
				ul.append('<li class="pop_error"><spring:message code="macErrorMsg1"></spring:message></li>');
				return false;
			} else {
				macFormat = checkNameIsExist("ip_table", mac, 3, opType);
				if (macFormat) {
					ul.append('<li class="pop_error"><spring:message code="macErrorMsg2"></spring:message></li>');
					return false;
				}
			}
			var note = "";
			if(opType == 'update'){
				note = $("#note_inp").val().replace(/(^\s*)|(\s*$)/g, "");
				if (note == "") {
					ul.append('<li class="pop_error"><spring:message code="noteErrorMsg1"></spring:message></li>');
					return false;
				}
			}

			if (!checkType)
				return false;
			var data = {};
			data.ipAddress = ip;
			data.batChid = batch;
			data.mac = mac;
			data.mask = mask;
			data.note = note;
			var resultJs = {};
			if (opType == "add") {
				data.opType = "add";
				 resultJs= ajaxWithServer(null, data, "insertIp");
				if (resultJs.result)
					window.location.href = "ipConfig";
				else
					swal("Message", resultJs.message);
			} else {
				data.opType = "update";
				data.id = $('#trIndex').val();
				resultJs = ajaxWithServer(null, data, "updateIp");
				if (resultJs.result)
					window.location.href = "ipConfig";
				else
					swal("Message", resultJs.message);
			}
		};
		
		function fillI18n(count){
			$("#selectedlen").text('<spring:message code="selectedNum" arguments="'+count+'"></spring:message>');
		};
		
		function setFile() {
			var fn = $("#fileid").val();
			if (fn.indexOf(".xls") <= 0 && fn.indexOf(".xlsx") <= 0
					&& fn.indexOf(".et") <= 0) {
				swal("Message","<spring:message code='choseExcel' ></spring:message>");
				return false;
			}

			var ajax_option = {
				url : "importIp",//默认是form action
				success : function(data) {
					var msg = $('#batchInsertMsg').val();
					// var msg = '总共-条数据，新增-条，修改-条，失败-条！';
					// var msg1 = '总共-条数据，新增-条，修改-条，失败-条！';
					temp = msg.split('-');
					
					swal(
							{
								title : "<spring:message code='ImportResult'></spring:message>",
								text : temp[0] + data.tottle + temp[1]
										+ data.insert + temp[2] + data.update + temp[3]
										+ data.error + temp[4] + "\n" + data.errMsg + "\n"
										+ data.errorList.join("\n"),
								type : "warning",
								showCancelButton : false,
								showConfirmButton:true,
								confirmButtonColor : "#2a94e4",
								confirmButtonText : "<spring:message code='Confirm'></spring:message>",
								closeOnConfirm : true
							},
							function(isConfirm) {
								window.location.href = "ipConfig";
							});
				},
				error : function(data) {
					swal("Message",
							"<spring:message code='uploadError' ></spring:message>");
				}
			};
			var form = $('#ipfileupload');
			form.ajaxSubmit(ajax_option);

		};
		function getSpringMsg(code,count){
			if(code =='ConfirmDeleteIps'){
				return '<spring:message code='ConfirmDeleteIps' arguments="'+count+'"></spring:message>';
			}else if(code =='Delete'){
				return '<spring:message code='Delete'></spring:message>';
			}else if(code =="okdelete"){
				return '<spring:message code='okdelete'></spring:message>';
			}else if(code =='cancel'){
				return '<spring:message code='cancel'></spring:message>';
			}else if(code =='ipHasInsMsg'){
				return '<spring:message code='ipHasInsMsg'></spring:message>';
			}
		}
	</script>
</body>
</html>
