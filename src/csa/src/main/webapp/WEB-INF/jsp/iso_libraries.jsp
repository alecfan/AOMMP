<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CSA</title>
		<link href="static/css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="static/css/sweetalert.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" type="text/css" href="static/css/webuploader/webuploader.css">
		<link href="static/css/base.css" rel="stylesheet" type="text/css" />
		<link href="static/css/main.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			.disableClick{ 
				pointer-events:none;
				cursor:default
			} 
		</style>
    </head>
    <body>
    
    <jsp:include page="include/nav.jsp">
		<jsp:param value="library" name="menu"/>
		<jsp:param value="uploadIso" name="menusub"/>
	</jsp:include>
	<!-- 模态声明，show 表示显示 -->
	<div class="modal fade" tabindex="-1" id="uploadIsoModal">
		<!-- 窗口声明 -->
		<div class="modal-dialog">
			<!-- 内容声明 -->
			<div class="modal-content">
				<!-- 头部 -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
					</button>
					<h4 class="modal-title">修改镜像信息</h4>
				</div>
				<!-- 主体 -->
				<div class="modal-body">
					<div class="container-fluid">
						<form id="isoForm" class="form-horizontal">
						<input type="hidden" id="statusBit" name="statusBit" value="">
						<input type="hidden" id="statusDescr" name="statusDescr" value="">
						  <div class="form-group">
						  	<div class="row">
							    <label for="" class="col-md-2 control-label">类型:</label>
							    <div class="col-md-4">
							      <select id="isoType" name="isoType" class="form-control">
							        <option value ="RHEL">RHEL</option>
							        <option value ="CEN">CEN</option>
							        <option value ="FED">FED</option>
							        <option value ="UBU">UBU</option>
							        <option value ="DEB">DEB</option>
							        <option value ="SUS">SUS</option>
							        <option value ="VMW">VMW</option>
							        <option value ="FBSD">FBSD</option>
							        <option value ="XEN">XEN</option>
							      </select>
							    </div>
							    <label for="" class="col-md-2 control-label">位数:</label>
							    <div class="col-md-4">
							      <select id="isoBit" name="isoBit" class="form-control">
							        <option value ="32">32</option>
							        <option value ="64">64</option>
							      </select>
							    </div>
						    </div>
						  </div>
						  <div class="form-group">
						  	<div class="row">
							    <label for="" class="col-md-2 control-label">系统版本:</label>
							    <div class="col-md-4">
							      <input type="text" id="isoVersion" class="form-control" name="isoVersion" placeholder="发行版版本号">
							    </div>
							    <label for="" class="col-md-2 control-label">用户版本:</label>
							    <div class="col-md-4">
							      <input type="text" id="isoUserVersion" class="form-control" name="isoUserVersion" placeholder="自定义版本号">
							    </div>
						    </div>
						  </div>
						  <div class="form-group">
						    <div class="row">
							    <label for="" class="col-md-2 control-label">用途:</label>
							    <div class="col-md-10">
							      <textarea id="useAim" name="useAim" class="form-control" rows="3"></textarea>
							    </div>
						    </div>
						  </div>
						</form>
					</div>
				</div>
				<!-- 注脚 -->
				<div class="modal-footer">
					<button id="saveIsoInfo" type="button" class="btn btn-primary">保存</button>
				</div>
			</div>
		</div>
	</div>
	
	<!--content-->
	<div class="content">
	            <!--头部，相册选择和格式选择-->
	            <div class="btn_search">
		            <div id="uploader">
		            	<!--用来存放文件信息-->
	                    <div id="thelist" class="uploader-list"></div>
	                        <div class="btns">
	                            <div id="picker"><spring:message code="UploadMirror"></spring:message></div>
	                        </div>
	                </div>
                </div>
	    <div class="csa_col3 bodrad5 bg_white">
	    	<h2 class="csa_title"><spring:message code="Mirroringbankaddress"></spring:message></h2>
	    	<ul class="csa_col3lst">
	    		<c:forEach items="${serverList}" var="server">
	    		<c:if test="${server.serverStatusMark eq '0'}">
				   <li><b>${server.ip}</b><span><spring:message code="Connected"></spring:message></span></li>
				</c:if>
				<c:if test="${server.serverStatusMark eq '1'}">
				   <li><b>${server.ip}</b><span class="dk"><spring:message code="Disconnected"></spring:message></span></li>
				</c:if>
	            </c:forEach>
	        </ul>
	    </div>
	    <!---->
	    <div class="csa_list bodrad5 bg_white">
	    	<h2 class="csa_title"><spring:message code="MirroringList"></spring:message></h2>
	    	<c:if test="${fn:length(typeList)>0}">
	        <div class="btn_search">
	        	<div class="csa_tab80 bodrad5 bg_white">
	        		<a class="current" href="${'#'}${typeList[0]}${'-pills'}" data-toggle="tab">${typeList[0]}</a>
	        		<c:forEach items="${typeList}" var="type" begin="1" >
	        			<a href="${'#'}${type}${'-pills'}" data-toggle="tab">${type}</a>
	        		</c:forEach>
		        	<!-- <a class="current" href="#rhel-pills" data-toggle="tab">RHEL</a>
		        	<a href="#CentOS-pills" data-toggle="tab">CentOS</a> -->
	        	</div>
	        </div>
	        </c:if>
	        
	        <!-- 标签面板 -->
            <div class="tab-content">
                <div class="tab-pane fade in active" id="${typeList[0]}${'-pills'}">
                    <div class="csa_table">
                    <c:choose>
			    	<c:when test="${fn:length(typeList)>0}">
			        	<div class="csa_tabletop" style="visibility:hidden;margin-left:40px;">
				        	<a name="Delete" class="bodrad5 edit" href="###"><spring:message code="Delete"></spring:message></a>
				        	<a name="Update" class="bodrad5 edit" href="###"><spring:message code="Update"></spring:message></a>
				        	<a name="Log" class="bodrad5 edit" href="###"><spring:message code="Log"></spring:message></a>
			        	</div>
			        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tr>
			                <th scope="col"><input id="${type}${'-checkbox'}" type="checkbox" value="" />&nbsp;<spring:message code="MirroringName"></spring:message></th>
			                <th scope="col"><spring:message code="MirroringStatus"></spring:message></th>
			                <th scope="col"><spring:message code="SystemVersion"></spring:message></th>
			                <th scope="col"><spring:message code="UserVersion"></spring:message></th>
			                <th scope="col"><spring:message code="Bit"></spring:message></th>
			                <th scope="col"><spring:message code="Account"></spring:message></th>
			                <th scope="col"><spring:message code="LastModified"></spring:message></th>
			                <th scope="col"><spring:message code="Use"></spring:message></th>
			              </tr>
			              <c:forEach items="${isoList}" var="iso" >
			              <c:if test="${iso.isoType == typeList[0] }">
			              <tr>
						   <td><input name="flag" id="${iso.csaId}" type="checkbox" value="" />&nbsp;${iso.isoName}</td>
						   <c:choose>
						   <c:when test="${iso.statusBit eq '01' or iso.statusBit eq '03'}">
						   <td style="color:#f9505a;">${iso.statusDescr}</td>
						   </c:when>
						   <c:otherwise>
						   <td style="color:#76c24e;">${iso.statusDescr}</td>
						   </c:otherwise>
						   </c:choose>
						   <td>${iso.isoVersion}</td>
						   <td>${iso.isoUserVersion}</td>
						   <td>${iso.isoBit}</td>
						   <td>${iso.userAccount}</td>
						   <td><fmt:formatDate value="${iso.createTime}"  type="date" pattern="yyyy-MM-dd HH:mm:ss"/></td>
						   <td>${iso.useAim}</td>
						  </tr>
			              </c:if>
			              </c:forEach>
			            </table>
				    </c:when>
				    <c:otherwise>
						<div class="alert alert-info" role="alert" style="margin-bottom: 0px;margin-top: 20px;"><strong>提示!</strong>没有相关数据</div>				    </c:otherwise>
				    </c:choose>
		        	</div>
                </div>
                <c:forEach items="${typeList}" var="type" begin="1" >
                <div class="tab-pane fade" id="${type}${'-pills'}">
					<div class="csa_table">
						<div class="csa_tabletop" style="visibility:hidden;margin-left:40px;">
						<a name="Delete" class="bodrad5 edit disableCss" href="###"><spring:message code="Delete"></spring:message></a>
						<a name="Update" class="bodrad5 edit" href="###"><spring:message code="Update"></spring:message></a>
						<a name="Log" class="bodrad5 edit" href="###"><spring:message code="Log"></spring:message></a>
						</div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						 <tr>
			                <th scope="col"><input id="${type}${'-checkbox'}" type="checkbox" value="" />&nbsp;<spring:message code="MirroringName"></spring:message></th>
			                <th scope="col"><spring:message code="MirroringStatus"></spring:message></th>
			                <th scope="col"><spring:message code="SystemVersion"></spring:message></th>
			                <th scope="col"><spring:message code="UserVersion"></spring:message></th>
			                <th scope="col"><spring:message code="Bit"></spring:message></th>
			                <th scope="col"><spring:message code="Account"></spring:message></th>
			                <th scope="col"><spring:message code="LastModified"></spring:message></th>
			                <th scope="col"><spring:message code="Use"></spring:message></th>
			              </tr>
			              <c:forEach items="${isoList}" var="iso" >
			              <c:if test="${iso.isoType eq type }">
							 <tr>
							   <td><input name="flag" id="${iso.csaId}" type="checkbox" value="" />&nbsp;${iso.isoName}</td>
							   <c:choose>
							   <c:when test="${iso.statusBit eq '01' or iso.statusBit eq '03'}">
							   <td style="color:#f9505a;">${iso.statusDescr}</td>
							   </c:when>
							   <c:otherwise>
							   <td style="color:#76c24e;">${iso.statusDescr}</td>
							   </c:otherwise>
							   </c:choose>
							   <td>${iso.isoVersion}</td>
							   <td>${iso.isoUserVersion}</td>
							   <td>${iso.isoBit}</td>
							   <td>${iso.userAccount}</td>
							   <td>${iso.createTime}</td>
							   <td>${iso.useAim}</td>
							 </tr>
						  </c:if>
						  </c:forEach>
						</table>
					</div>
                </div>
                </c:forEach>
            </div>
	    </div>
	</div>
	<!--footer-->
	<div class="cas_foot"><span class="mr30">版权所有 © 2016</span>神州泰岳 UltraPower</div>
	<script type="text/javascript" src="static/js/jquery-2.2.1.min.js" ></script>
	<script src="static/js/sweetalert.min.js"></script>
	<script src="static/js/jquery.form.min.js"></script>
	<script src="static/js/jquery.validate.js"></script>
	<script type="text/javascript" src="static/css/bootstrap/js/bootstrap.js" ></script>
	<script type="text/javascript" src="static/js/webuploader/webuploader.js"></script>
    <script type="text/javascript" src="static/js/webuploader/webuploader_up.js"></script>
    <!-- <script type="text/javascript" src="static/js/effects.js"></script> -->
	<script type="text/javascript">
		$(document).ready(function(){
			$("a[name='Update']").click(function() {
				$.ajax({
                    type: 'POST',
                    url: 'findIsoById',
                    data:{"isoId": $(".tab-pane.fade.in.active input:checkbox:checked[name='flag']").attr('id') },
                    dataType:"json",
                    success: function(data) {
                    	$('#isoType').val(data.isoType);
                    	$('#isoBit').val(data.isoBit);
                    	$('#isoVersion').val(data.isoVersion);
                    	$('#isoUserVersion').val(data.isoUserVersion);
                    	$('#useAim').val(data.useAim);
                    	if(data.statusBit=='0'){
                    		$('#statusBit').val('00');
                    		$('#statusDescr').val('上传中');
                    	}
                    }
                });
				$('#uploadIsoModal').modal({
					backdrop : 'static',
					keyboard : false,
				});
	        });
			$('#uploadIsoModal').on('hidden.bs.modal', function (e) {
				$("label.error").hide();
 				$(".error").removeClass("error");
				$(this).removeData("bs.modal");
			})
			$("a[name='Delete']").click(function() {
	            swal({
	                title: "确认删除所选镜像？",
	                text: "",
	                type: "warning",
	                showCancelButton: true,
	                closeOnConfirm: false,
	                confirmButtonText: "是的，我要删除",
	                confirmButtonColor: "#2a94e4"
	            }, function() {
	            	var ids = [];   
	            	$(".tab-pane.fade.in.active input:checkbox:checked[name='flag']").each(function() {   
	            		ids.push($(this).attr('id'));
	                }); 
	                $.ajax({
	                    type: 'POST',
	                    url: 'deleteiso',
	                    data: JSON.stringify(ids),
	                    dataType:"json",
	                    contentType:"application/json",
	                    success: function(data) {
	                    	if('true'==(data+'')){
	                    		swal('发起删除操作成功!', '', 'success');
	                    		location.reload();
	                    	}
	                    },
		                error: function(data) {
	                    		swal('发起删除操作失败!', '', 'error');
	                    }
	                }); 
	            });
	        });
			$('#saveIsoInfo').click(function() {
				if(JqValidate()){
					$("#isoForm").ajaxSubmit({
					     type: "post",
					     url: "saveIsoInfo",
					     data: {
					         'csaId' : $(".tab-pane.fade.in.active input:checkbox:checked[name='flag']").attr('id')
					     },
					     dataType: "json",
					     success: function(result){
					    	 $('#uploadIsoModal').modal('hide');
					    	 swal({   title: "修改操作成功!",   text: "",   type: "success",   showCancelButton: false,   confirmButtonColor: "#2a94e4",   confirmButtonText: "确定!",   closeOnConfirm: false }, function(){   location.reload(); });
					     }
					 });
			    }
			});
			//标签页
			$('.csa_tab80').on('click','a', function() {
				$('a.current').removeClass('current');
				$(this).addClass('current');
			});
			//定时更新镜像库和镜像状态
			setInterval(function(){
				$.ajax({  
			        type:'GET',
			        dataType:'json',
			        url:'updateServerStatus',
			        success:function(data){
			        	var serverList = data.serverList;
			        	var isoList = data.isoList;
			        	$.each(serverList, function(i) {
			        		var obj = this;
			        		var ipTemp = this.ip;
			        		var serverStatusMarkTemp = this.serverStatusMark;
			        		$('.csa_col3lst li').each(function(){
			        			if($(this).find('b').text()==ipTemp){
			        				if(serverStatusMarkTemp=='0'){
			        					$(this).find('span').removeClass('dk').html('<spring:message code="Connected"></spring:message>');
			        				}else{
			        					$(this).find('span').addClass('dk').html('<spring:message code="Disconnected"></spring:message>');
			        				}
			        			}
			        		});
	                    });
			        	$.each(isoList, function(i) {
			        		var idTemp = this.csaId;
			        		var statusBit = this.statusBit;
			        		var statusDescr = this.statusDescr;
			        		$('.tab-pane.fade.in.active tr td:nth-child(2)').each(function(){
			        			if($(this).parent().find('input').attr('id')==idTemp&&$(this).text()!='不可用'){
			        				if(statusBit=='00'||statusBit=='02'||statusBit=='10'){
			        					$(this).css("color","#76c24e").text(statusDescr);
			        				}else{
			        					$(this).css("color","#f9505a").text(statusDescr);
			        				}
			        			}
			        		});
	                    });
			        }
			        });
			},10000);
			//按钮组动态显示
			$(document).on("click","input[type='checkbox']", function() {
			 	if($(this).prop('checked')&&$(this).attr('id').indexOf('-checkbox')>=0){
			 		$(".tab-pane.fade.in.active input[type='checkbox']").prop("checked",true);
			 	}else if(!$(this).prop('checked')&&$(this).attr('id').indexOf('-checkbox')>=0){
			 		$(".tab-pane.fade.in.active input[type='checkbox']").prop("checked",false);
			 	}
				var len = $(".tab-pane.fade.in.active input:checkbox:checked[name='flag']").length;
				if(len<1){
					$(".tab-pane.fade.in.active .csa_tabletop").css("visibility","hidden");
					$(".tab-pane.fade.in.active input[type='checkbox']").prop("checked",false);
				}else{
					$(".tab-pane.fade.in.active .csa_tabletop").css("visibility","visible");
					if(len>1){
						$(".tab-pane.fade.in.active a[name='Log']").removeClass('edit').addClass('disableClick');
						$(".tab-pane.fade.in.active a[name='Update']").removeClass('edit').addClass('disableClick');
					}else{
						$(".tab-pane.fade.in.active a[name='Log']").addClass('edit').removeClass('disableClick');
						$(".tab-pane.fade.in.active a[name='Update']").addClass('edit').removeClass('disableClick');
					}
				}
		    })
		});
		function JqValidate(){
		    return $('#isoForm').validate({
		    	rules:{
					useAim:{
                        required:true
                    },
                    isoVersion:{
                    	required:true
		            },
		            isoUserVersion:{
		                required:true
		            }
                },
                messages:{
                	useAim:{
                        required:'必填'
                    },
                    isoVersion:{
                    	required:'必填'
	                },
	                isoUserVersion:{
	                    required:'必填'
	                }
                }
		    }).form();  
		}  
	</script>
 	</body>
</html>
