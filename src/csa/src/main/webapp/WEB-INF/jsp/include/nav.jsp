<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="header">
	<div class="nav">
        <h1 class="logo">Ultra-CSA</h1>
        <ul class="menu">
        <c:forEach items="${sessionScope.Permissions}" var="Permissions">
	        <c:choose>
			    <c:when test="${fn:length(Permissions)==1}">
			    	<li class="${param.menu eq Permissions[0]?'current':''}"><a href="${pageContext.request.contextPath}/${Permissions[0]}"><i class="${Permissions[0]}"></i><spring:message code="${Permissions[0]}"></spring:message><s></s></a></li>
			    </c:when>
			    <c:otherwise>
			    	<li class="${param.menu eq Permissions[0]?'current':''}"><a href="${pageContext.request.contextPath}/${Permissions[1]}"><i class="${Permissions[0]}"></i><spring:message code="${Permissions[0]}"></spring:message><s></s></a></li>
			    </c:otherwise>
		    </c:choose>
	    </c:forEach>
        <%-- 
            <li class="${param.menu eq 'Home'?'current':''}"><a href="${pageContext.request.contextPath}/home"><i></i><spring:message code="Home"></spring:message><s></s></a></li>
            <li class="${param.menu eq 'Deploy'?'current':''}"><a href="${pageContext.request.contextPath}/installProgram"><i></i>OS<spring:message code="Deploy"></spring:message><s></s></a></li>
            <li class="${param.menu eq 'Maintain'?'current':''}"><a href="#"><i></i><spring:message code="Maintain"></spring:message><s></s></a></li>
            <li class="${param.menu eq 'Collection'?'current':''}"><a href="#"><i></i><spring:message code="Collection"></spring:message><s></s></a></li>
            <li class="${param.menu eq 'library'?'current':''}"><a href="${pageContext.request.contextPath}/uploadIso"><spring:message code="Library"></spring:message><s></s></a></li> --%>
        </ul>
        <div class="user"><span>${sessionScope.userName}</span><span class="user_pic"><img src="static/img/user_pic.png" /></span></div>
    </div>

	<c:forEach items="${sessionScope.Permissions}" var="Permissions" begin="1">
		<c:if test="${param.menu eq Permissions[0]}">
		<div class="menusub">
			<ul>
				<c:forEach items="${Permissions}" var="Permission" begin="1">
					<li class="${param.menusub eq Permission?'current':''}"><a href="${pageContext.request.contextPath}/${Permission}"><i class="${Permission}"></i><spring:message code="${Permission}"></spring:message></a></li>
				</c:forEach>
			</ul>
		</div>
		</c:if>
	</c:forEach>

	<%-- <c:if test="${param.menu eq 'Deploy'}">
		<div class="menusub">
			<ul>
				<li class="${param.menusub eq 'ipconfig'?'current':''}"><a href="${pageContext.request.contextPath}/ipConfig"><i></i><spring:message code="ipconfig"></spring:message></a></li>
				<li class="${param.menusub eq 'installProgram'?'current':''}"><a href="${pageContext.request.contextPath}/installProgram"><i></i>
						<spring:message code="installscheme"></spring:message></a></li>
				<li class="${param.menusub eq 'installDetails'?'current':''}"><a href="${pageContext.request.contextPath}/installDetails"><i></i>
						<spring:message code="installDetail"></spring:message></a></li>		
				<li  class="${param.menusub eq 'Osdeploy'?'current':''}"><a href="${pageContext.request.contextPath}/osdeploy"><i></i>
						<spring:message code="OSDeploy"></spring:message></a></li>
			</ul>
		</div>
	</c:if>
	
	<c:if test="${param.menu eq 'library'}">
		<div class="menusub">
	        <ul>
	            <li class="${param.menusub eq 'mirror'?'current':''}"><a href="${pageContext.request.contextPath}/uploadIso"><i></i><spring:message code="Mirror"></spring:message></a></li>
	            <li><a href="javascript:void(0)"><i></i><spring:message code="Software"></spring:message></a></li>
	            <li><a href="${pageContext.request.contextPath}/shellview"><i></i><spring:message code="Script"/></a></li>
	        </ul>
	    </div>
    </c:if> --%>
    
</div>
